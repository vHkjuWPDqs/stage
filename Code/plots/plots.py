from helmholtz.domain_decomposition_solvers.iterative_solvers import AnalyticJacobiDDMSolver
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import matplotlib.animation as animation
from scipy.linalg import eig, svd
import numpy as np
import scipy
from sklearn.linear_model import LinearRegression
import matplotlib
from cycler import cycler
from collections import OrderedDict



class GMRES_CALLBACK:
        def __init__(self):
            self.res=[]
    
        def __call__(self, args):
            self.res.append(args)

#Permet l'affichage des résultats, un objet AnalyticJacobiDDMSolver est contenu dans PLOT, après modification des self.xxx 
# il faut faire un self.updateSolver() afin de mettre à jour le solveur

class PLOTS:
    def __init__(self, N, Z):
        self.Zsauv = Z
        self.Z=Z
        self.omega = 10
        self.a = 0
        self.b = 1
        self.N = N
        self.Nsauv=N
        self.g = [-self.omega*2j*np.exp(self.omega*1j*self.a), 0.0]

        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)
    
    def updateSolver(self):
        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)

    def reset(self):
        self.Z = self.Zsauv
        self.omega = 10
        self.a = 0
        self.b = 1
        self.N = self.Nsauv
        self.g = [-self.omega*2j*np.exp(self.omega*1j*self.a), 0.0]

        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)

    def printParam(self):
        print("Omega : %f, a : %f, b : %f, N : %f"%(self.omega, self.a, self.b, self.N))
        print("Z : ")
        print(self.Z)
    
    #Valeurs propres de A=Id + PI o S
    def plotAEigenValues(self):
        self.reset()

        ZA = self.analyticJDDsolver.A#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [DA, PA] = eig(ZA)#, self.analyticJDDsolver.ZMat)

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        circle = plt.Circle((1.0, 0.0), 1.0, color='r', alpha=0.2)
        ax.add_artist(circle)
        ax.plot()
        ax.grid()        
        ax.axis('equal')
        plt.title('Valeurs propres de $I+\Pi S$ (Cercle unité centré en (1,0) en rouge')
        sc = ax.scatter(DA.real, DA.imag)

        plt.xlim(-0.5,2.5)
        plt.ylim(-1.5,1.5)

        axOmega = plt.axes([0.25, .07, 0.50, 0.02])
        axN = plt.axes([0.25, .03, 0.50, 0.02])
        axA = plt.axes([0.25, .11, 0.50, 0.02])
        axD = plt.axes([0.25, .14, 0.50, 0.02])
        axBr = plt.axes([0.25, .17, 0.50, 0.02])
        axBtheta = plt.axes([0.25, .21, 0.50, 0.02])
        axa = plt.axes([0.25, .24, 0.50, 0.02])
        axL = plt.axes([0.25, .27, 0.50, 0.02])
        # Slider
        sOmega = Slider(axOmega, '$\omega$', 5.0, 100.0, valinit=self.omega, valstep=0.1)
        sN = Slider(axN, '$N$', 2.0, 100.0, valinit=self.N, valstep=1.0)
        sA = Slider(axA, '$z_{00}$', 0.01, 100.0, valinit=1.0, valstep=0.1)
        sD = Slider(axD, '$z_{11}$', 0.01, 100.0, valinit=1.0, valstep=0.1)
        sBtheta = Slider(axBtheta, '$\\theta_{Z_{01}}$', 0.0, 6.28318530718, valinit=0.0, valstep=0.01)
        sBr = Slider(axBr, '$r_{Z_{01}}$', 0.001, 0.999, valinit=0.0, valstep=0.001)
        sa = Slider(axa, '$a$', -10.0, 10.0, valinit=self.analyticJDDsolver.x[0], valstep=0.1)
        sL = Slider(axL, '$L=b-a$', 0.1, 10.0, valinit=self.analyticJDDsolver.x[-1]-self.analyticJDDsolver.x[0], valstep=0.1)

        def update(val):
            self.omega = sOmega.val

            Zloc = np.zeros((2,2), dtype=complex)
            Zloc[0][0]=sA.val
            Zloc[1][1]=sD.val
            Zloc[0][1]=np.sqrt(sA.val*sD.val)*sBr.val*np.exp(1j*sBtheta.val)
            Zloc[1][0]=Zloc[0][1].conj()

            print("Zloc : ")
            print(Zloc)

            self.N = int(sN.val)
            self.Z = [np.array(Zloc)]*self.N
            self.a = sa.val
            self.b = sa.val + sL.val
            self.g = [-self.omega*2j*np.exp(self.omega*1j*self.a), self.omega*2j*np.exp(-self.omega*1j*self.b)]          
            self.updateSolver()

            ZA = self.analyticJDDsolver.A
            [DA, PA] = eig(ZA)

            sc.set_offsets(np.c_[DA.real, DA.imag])
            figure.canvas.draw_idle()
        
        sOmega.on_changed(update)
        sN.on_changed(update)
        sA.on_changed(update)
        sD.on_changed(update)
        sBtheta.on_changed(update)
        sBr.on_changed(update)
        sa.on_changed(update)
        sL.on_changed(update)
        plt.show()

    #Svd de A dans le produit scalaire Z
    def plotAEigenValuesIdDtN(self):
        self.reset()

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        circle = plt.Circle((1.0, 0.0), 1.0, color='r', alpha=0.2)
        ax.add_artist(circle)
        ax.plot()
        ax.grid()        
        ax.axis('equal')

        

        Ztemp = [\
        1/np.sinh(self.omega/(self.N-1))*np.array([[np.cosh(self.omega/(self.N-1)), -1.0],\
                            [-1.0, np.cosh(self.omega/(self.N-1))]])\
        ]
        self.Z = Ztemp*(self.N-1)
        self.updateSolver()

        U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

        UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [QA, DA, PA] = svd(UAUm, full_matrices=True)

        gammaDtN = min(DA)


        ZA = self.analyticJDDsolver.A#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [DA, PA] = eig(ZA)#, self.analyticJDDsolver.ZMat)

        ax.scatter(DA.real, DA.imag, label='$Z=DtN$', s=2)
        
        Ztemp = [np.array([\
        [1.0, 0.0],\
        [0.0, 1.0]\
        ])]
        self.Z = Ztemp*(self.N-1)
        self.updateSolver()

        U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

        UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [QA, DA, PA] = svd(UAUm, full_matrices=True)

        gammaId = min(DA)

        ZA = self.analyticJDDsolver.A#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [DA, PA] = eig(ZA)#, self.analyticJDDsolver.ZMat)

        circle = plt.Circle((0.0, 0.0), gammaDtN**2/2, color='b', alpha=0.2)
        ax.add_artist(circle)
        ax.plot(label='Cercle de rayon $\frac{\gamma_{DtN}^2}{2}$')

        circle = plt.Circle((0.0, 0.0), gammaId**2/2, color='g', alpha=0.2)
        ax.add_artist(circle)
        ax.plot(label='Cercle de rayon $\frac{\gamma_{\omega I}^2}{2}$')
        


        ax.scatter(DA.real, DA.imag, label='$Z=\omega I$', s=2)

        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))

        plt.show()


    #Même chose avec comparaison Z=DtN , Z=Id
    def plotSVDIdAndDtN(self):
        self.reset()

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        ax.grid()  

        Ztemp = [\
        1/np.sinh(self.omega/(self.N-1))*np.array([[np.cosh(self.omega/(self.N-1)), -1.0],\
                            [-1.0, np.cosh(self.omega/(self.N-1))]])\
        ]
        self.Z = Ztemp*(self.N-1)
        self.updateSolver()

        U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

        UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [QA, DA, PA] = svd(UAUm, full_matrices=True)

        plt.semilogy(range(len(DA)), DA, label="$Z=DtN$", color="C0")
        
        gammaDtN=min(DA)

        Ztemp = [np.array([\
        [1.0, 0.0],\
        [0.0, 1.0]\
        ])]
        self.Z = Ztemp*(self.N-1)
        self.updateSolver()

        U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

        UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
        [QA, DA, PA] = svd(UAUm, full_matrices=True)

        ax.semilogy(range(len(DA)), DA, label="$Z=\omega I$", color="C1")
        
        gammaId=min(DA)

        ax.scatter([0], gammaDtN, label="$\gamma_{DtN}$", color="C0")
        ax.scatter([0], gammaId, label="$\gamma_{\omega I}$", color="C1")
        ax.set_yscale("log")

        ax.plot([0, len(DA)], [gammaDtN, gammaDtN], "C0--")
        ax.plot([0, len(DA)], [gammaId, gammaId], "C1--")

        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.show()

    #not used
    def solveGMRES(self, N_it = 100, eps = 1e-6):

        cb = GMRES_CALLBACK()

        self.analyticJDDsolver.trans = scipy.sparse.linalg.gmres(self.analyticJDDsolver.A, -self.analyticJDDsolver.Pi.dot(self.analyticJDDsolver.f), maxiter=N_it, tol=eps, callback=cb)
        return [self.analyticJDDsolver.trans, cb.res]


    #Convergence 3D

    # def plotConvergence(self):
    #     self.reset()

    #     self.analyticJDDsolver.solve(0)

    #     x = np.linspace(self.a, self.b, 1000)
    #     y = np.array([self.analyticJDDsolver(xi) for xi in x], dtype=complex)

    #     figure, ax = plt.subplots(subplot_kw=dict(projection='3d'))
    #     plt.subplots_adjust(left=0.25, bottom=0.15)

    #     plt.title('Convergence de l\'algorithme')
    #     sc = ax.scatter(x, y.real, y.imag, s=0.5)

    #     yanalytic = np.array([np.exp(1j*self.omega*xi) for xi in x], dtype=complex)
    #     ax.scatter(x, yanalytic.real, yanalytic.imag, s=0.5)

    #     axit = plt.axes([0.25, .03, 0.50, 0.02])
    #     # Slider
    #     sit = Slider(axit, '$Nombre d\'itérations$', 0.0, 100.0, valinit=0.0, valstep=1.0)

    #     def update(val):
    #         it = int(sit.val)        
    #         self.updateSolver()
    #         self.analyticJDDsolver.solve(it)

    #         err = np.linalg.norm(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans)/np.linalg.norm(self.analyticJDDsolver.analytic_trans)

    #         plt.title('Erreur relative %f' %(err))

    #         y = np.array([self.analyticJDDsolver(xi) for xi in x], dtype=complex)

    #         sc._offsets3d=(x, y.real, y.imag)
    #         figure.canvas.draw_idle()
        
    #     sit.on_changed(update)
    #     plt.show()


    #Convergence 2D: (real, im)
    def plotConvergence(self):
        self.reset()
        # plt.ion()
        self.analyticJDDsolver.solve(0)

        x = np.linspace(self.a, self.b, 1000)
        y = np.array([self.analyticJDDsolver(xi) for xi in x], dtype=complex)

        # figure, ax = plt.subplots(subplot_kw=dict(projection='3d'))
        figure, ax = plt.subplots()
        figure2, ax2 = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.15)

        yanalytic1 = np.array([np.cos(self.omega*xi) for xi in x])
        yanalytic2 = np.array([np.sin(self.omega*xi) for xi in x])
    
        # Slider
        ax.set_prop_cycle(cycler('color', ['pink', 'magenta', 'purple', 'blue', 'cyan']))
        ax2.set_prop_cycle(cycler('color', ['pink', 'magenta', 'purple', 'blue', 'cyan']))

        for it in [0,4,8,20,50]:      
            self.reset()
            
            self.analyticJDDsolver.solve(it, 1E-14)

            y1 = np.array([self.analyticJDDsolver(xi).real for xi in x])
            y2 = np.array([self.analyticJDDsolver(xi).imag for xi in x])

            ax.scatter(x, y1, label='$\Re (u^{(%i)})$' % it, s=0.3)
            ax2.scatter(x, y2, label='$\Im (u^{(%i)})$ ' % it, s=0.3)

        

        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05),scatterpoints=10)
        ax2.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05),scatterpoints=10)

        plt.show()
    
    #Graph d'erreur relative à chaque itération en norme Z (commenter / décommenter Id ou DtN)
    def plotErrorGraph(self):
        it  = range(1000)
        # err = np.zeros(len(it))

        self.reset()

        for J in range(5,51,10):
            Nsub = 2*J
            self.N=Nsub+1
            # Ztemp = [\
            # 1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
            #                     [-1.0, np.cosh(self.omega/Nsub)]])\
            # ]
            Ztemp = [np.array([\
            [1.0, 0.0],\
            [0.0, 1.0]\
            ])]
            self.Z = Ztemp*Nsub

            self.updateSolver()

            errZ = np.zeros(len(it))
            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)
            for i in it:
                self.analyticJDDsolver.iterate()
                # err[i]=np.linalg.norm(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans)/np.linalg.norm(self.analyticJDDsolver.analytic_trans)
                errZ[i]=np.linalg.norm(U.dot(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans))/np.linalg.norm(U.dot(self.analyticJDDsolver.analytic_trans))
        
            plt.semilogy(it, errZ, label='$J = %i$' % Nsub)

        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.xlabel("Nombre d'itérations", fontsize=14)
        plt.ylabel("Erreur relative", fontsize=14)
        plt.grid()
        plt.show()

    #même chose mais avec les deux d'un coup
    def plotErrorGraphIdAndDtN(self):
        it  = range(1000)
        # err = np.zeros(len(it))

        self.reset()

        for J in range(50,51,10):
            Nsub = 2*J
            self.N=Nsub+1
            Ztemp = [\
            1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                [-1.0, np.cosh(self.omega/Nsub)]])\
            ]
            self.Z = Ztemp*Nsub

            self.updateSolver()

            errZ = np.zeros(len(it))
            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)
            for i in it:
                self.analyticJDDsolver.iterate()
                # err[i]=np.linalg.norm(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans)/np.linalg.norm(self.analyticJDDsolver.analytic_trans)
                errZ[i]=np.linalg.norm(U.dot(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans))/np.linalg.norm(U.dot(self.analyticJDDsolver.analytic_trans))
        
            plt.semilogy(it, errZ, label='$Z=DtN$')

            Ztemp = [np.array([\
            [1.0, 0.0],\
            [0.0, 1.0]\
            ])]

            self.Z = Ztemp*Nsub

            self.updateSolver()

            errZ = np.zeros(len(it))
            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)
            for i in it:
                self.analyticJDDsolver.iterate()
                # err[i]=np.linalg.norm(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans)/np.linalg.norm(self.analyticJDDsolver.analytic_trans)
                errZ[i]=np.linalg.norm(U.dot(self.analyticJDDsolver.trans-self.analyticJDDsolver.analytic_trans))/np.linalg.norm(U.dot(self.analyticJDDsolver.analytic_trans))
        
            plt.semilogy(it, errZ, label='$Z=\omega I$')

        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.xlabel("Nombre d'itérations", fontsize=14)
        plt.ylabel("Erreur relative", fontsize=14)
        plt.grid()
        plt.show()

    #Permet d'afficher la cst inf sup avec cdt de Neumann (calcul analytique, voir rapport)
    def plotInfSupNeumannByFq(self):
        fq=[i/1000 for i in range(5000,100000)]
        a=np.zeros(len(fq))
        b=np.zeros(len(fq))
        c=np.zeros(len(fq))
        borne=np.zeros(len(fq))
        borne2=np.zeros(len(fq))
        i=0
        L=1
        for w in fq:
            k0=np.floor(w*L/np.pi)
            a[i]=abs(((k0*np.pi/L)**2-w**2)/((k0*np.pi/L)**2+w**2))
            k0+=1
            b[i]=abs(((k0*np.pi/L)**2-w**2)/((k0*np.pi/L)**2+w**2))
            c[i]=min(a[i], b[i])
            borne[i]=1/(2*w*L/np.pi-1)
            borne2[i]=1/(2*k0-1)
            i+=1
        # plt.plot(fq, a, color='C0')
        # plt.plot(fq, b, color='C1')
        plt.xlabel("$\\omega$")
        plt.ylabel("$\\alpha$")
        plt.plot(fq, c, color='C0', label="$\\alpha$")
        plt.plot(fq, borne, '--', color='C1', label="$\\frac{1}{2\\frac{\\omega L}{\\pi}-1}$")
        plt.plot(fq, borne2, '--', color='C2', label="$\\frac{1}{2\\left\\lfloor\\frac{\\omega L}{\\pi}\\right\\rfloor+1}$")
        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.show()

    #Erreur sur u !!! (en norme h1 et plus sur p)
    def plotErrorGraphFuncIdAndDtN(self):
        plt.xlabel("Nombre d'itérations")
        plt.ylabel("$\\frac{\\left\\|u^{(n)}-u_\\infty\\right\\|_{\\mathbb{H}^1(I)}}{\\left\\|p^{(n)}-p_\\infty\\right\\|}$")
        plt.grid()
        
        iterations = range(1000)

        self.reset()
        Nsub = self.analyticJDDsolver.N-1
        Ztemp = [np.array([\
            [1.0, 0.0],\
            [0.0, 1.0]\
            ])]

        self.Z = Ztemp*Nsub

        self.updateSolver()
        
        normH1Id=np.zeros(len(iterations))
        for it in range(len(iterations)):
            normH1Id[it]=self.analyticJDDsolver.getH1NormDiff()
            self.analyticJDDsolver.iterate()
            p=self.analyticJDDsolver.trans
        #DtN
        self.reset()
        Nsub = self.analyticJDDsolver.N-1
        Ztemp = [\
        1/np.sinh(self.omega/(self.N-1))*np.array([[np.cosh(self.omega/(self.N-1)), -1.0],\
                            [-1.0, np.cosh(self.omega/(self.N-1))]])\
        ]

        self.Z = Ztemp*Nsub

        self.updateSolver()

        normH1DtN=np.zeros(len(iterations))
        for it in range(len(iterations)):
            normH1DtN[it]=self.analyticJDDsolver.getH1NormDiff()
            self.analyticJDDsolver.iterate()
            p=self.analyticJDDsolver.trans

        
        plt.semilogy(iterations, normH1DtN, color='C0', label="$Z=DtN$")
        plt.semilogy(iterations, normH1Id, color='C1', label="$Z=\omega I$")

        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))

        plt.show()


    #Gamma par SVD en fonction de nombre de sous-domaines
    def plotGammaDeteriorationIdAndDtN(self):

        self.reset()

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        ax.grid()

        subs = range(2,100,5)

        gammaDtNTab=np.zeros(len(subs))
        gammaIdTab=np.zeros(len(subs))

        lminDtNTab=np.zeros(len(subs))
        lminIdTab=np.zeros(len(subs))
        
        i=0

        for Nsub in subs:
            self.N=Nsub+1
            print(Nsub)
            Ztemp = [\
            1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                [-1.0, np.cosh(self.omega/Nsub)]])\
            ]
            self.Z = Ztemp*Nsub

            self.updateSolver()

            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

            UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
            [QA, DA, PA] = svd(UAUm, full_matrices=True)
            gammaDtNTab[i]=min(DA)
            [DA, PA]=eig(self.analyticJDDsolver.A)
            lminDtNTab[i]=min(abs(DA))
            i+=1
        
        i=0
        self.reset()
        for Nsub in subs:
            self.N=Nsub+1
            print(Nsub)
            Ztemp = [np.array([\
            [1.0, 0.0],\
            [0.0, 1.0]\
            ])]
            self.Z = Ztemp*Nsub

            self.updateSolver()

            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

            UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
            [QA, DA, PA] = svd(UAUm, full_matrices=True)
            gammaIdTab[i]=min(DA)
            [DA, PA]=eig(self.analyticJDDsolver.A)
            lminIdTab[i]=min(abs(DA))
            i+=1

        ax.plot(subs, gammaDtNTab, 'C0', label="$\gamma_{DtN}$")
        ax.plot(subs, gammaIdTab, 'C1', label="$\gamma_{\omega I}$")

        ax.plot(subs, lminDtNTab, 'C2.-', label="$\lambda_{min}$ (DtN)")
        ax.plot(subs, lminIdTab, 'C3.-', label="$\lambda_{min}$ ($\omega I$)")

        subsInterp=[[i] for i in subs[10:len(subs)]]
        invGammaDtN=[1/i for i in gammaDtNTab]
        invGammaId=[1/i for i in gammaIdTab]

        modelRegDtN=LinearRegression()
        modelRegDtN.fit(subsInterp, invGammaDtN[10:len(subs)])

        yDtN=[1/(modelRegDtN.coef_*i+modelRegDtN.intercept_) for i in subs]

        modelRegId=LinearRegression()
        modelRegId.fit(subsInterp, invGammaId[10:len(subs)])

        yId=[1/(modelRegId.coef_*i+modelRegId.intercept_) for i in subs]

        # ax.plot(subs, invGammaDtN)
        # ax.plot(subs, invGammaId)
        ax.plot(subs, yDtN, 'C0--', label="$Z=DtN$, coeff=%f" % modelRegDtN.coef_)
        ax.plot(subs, yId, 'C1--', label="$Z=\omega I$, coeff=%f" % modelRegId.coef_)

        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))

        plt.show()

    #Borne théorique Gamma
    def plotGammaDeteriorationAndTheoricalDeterioration(self):
        self.reset()

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        ax.grid()

        subs = range(2,3003,500)

        gammaDtNTab=np.zeros(len(subs))
        
        i=0

        # def f(x):
        #     return (1-2*self.omega**2*(1+(x*np.pi/self.)))

        for Nsub in subs:
            self.N=Nsub+1
            Ztemp = [\
            1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                [-1.0, np.cosh(self.omega/Nsub)]])\
            ]
            self.Z = Ztemp*Nsub

            self.updateSolver()

            U = scipy.linalg.cholesky(self.analyticJDDsolver.ZMat, lower=False)

            UAUm = U.dot(self.analyticJDDsolver.A).dot(np.linalg.inv(U))#self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.A)
            [QA, DA, PA] = svd(UAUm, full_matrices=True)
            gammaDtNTab[i]=min(DA)


            i+=1
        #Partie analytique
        w=self.analyticJDDsolver.omega
        L=self.analyticJDDsolver.x[-1]-self.analyticJDDsolver.x[0]

        na=2+1/self.analyticJDDsolver.omega * (np.cosh(w*L)+1)/(np.cosh(w*L)-1)
        alpha=1/np.sqrt(2+4*L*L*w*w)

        gammaTheoTab=np.sqrt(2)*alpha/(1+4*na**2)#lam+ + a rho / lam- <= 1+(1+1/w) pour norme h1 = omega^2 norm l2 + norm grad l2
        
        print(gammaTheoTab)

        ax.plot(subs, [1/i for i in gammaDtNTab], 'C0', label="$\gamma_{DtN}$")
        ax.plot([subs[0], subs[-1]], [1/gammaTheoTab, 1/gammaTheoTab], 'C1', label="Borne inférieure pour $\gamma_{DtN}$")

        plt.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))

        plt.show()

    #Nb d'it pour converger en fonction du nb de sous-domaines
    def plotIterationsBeforeConvergenceByNBSubDomains(self):
        self.reset()

        matplotlib.rcParams.update({'font.size': 14})

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        lines = ax.plot()
        lines2 = lines.copy()
        ax.grid()        
        plt.xlabel("Nombre de sous-domaines", fontsize=14)
        plt.ylabel("Nombre d'itérations", fontsize=14)

        # subs=range(2,150)
        subs = list(OrderedDict.fromkeys([int(i)+1 for i in np.logspace(0, 2.5, num=100)]))
        nbIt=np.zeros(len(subs))
        for w in range(10,101,40):
            self.omega = w
            print(".")
            for i in range(len(subs)):
                Nsub = subs[i]            
                self.N = Nsub+1

                # Ztemp = [\
                # 1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                #                     [-1.0, np.cosh(self.omega/Nsub)]])\
                # ]
                Ztemp = [np.array([\
                [3, 0.0],\
                [0.0, 3]\
                ])]
                self.Z = Ztemp*Nsub
                # self.Z[0][0][0]=1
                # self.Z[-1][1][1]=1
                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solve(100000, 1e-4)
                nbIt[i]=it

            lines+=plt.plot([1/i for i in subs], nbIt)

            # X=[[i] for i in subs[110:len(subs)]]
            # y=[i for i in nbIt[110:len(nbIt)]]

            # modelReg=LinearRegression()
            # modelReg.fit(X, y)

            # # print(modelReg.intercept_)
            # # print(modelReg.coef_)

            # #calcul du R²
            # # print(modelReg.score(X,y))
            # print(modelReg.coef_)
            # y=[modelReg.coef_*i+modelReg.intercept_ for i in subs]
            # lines2+=plt.plot(subs, y, '--')

        matplotlib.rcParams.update({'font.size': 10})
        legend1 = ax.legend(lines[:], ['w=10', 'w=50', 'w=90'],
          loc='upper right', bbox_to_anchor=(1.1, 1.05))
        ax.legend(lines2[:], ['w=10', 'w=50', 'w=90'],
          loc='lower right', bbox_to_anchor=(1.1, -0.05))
        plt.gca().add_artist(legend1)
        plt.show()

    #Même chose mais avec l'erreur sur u et plus sur p
    def plotIterationsBeforeConvergenceFuncByNBSubDomains(self):
        self.reset()

        matplotlib.rcParams.update({'font.size': 14})

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        lines = ax.plot()
        lines2 = lines.copy()
        ax.grid()        
        plt.xlabel("Nombre de sous-domaines", fontsize=14)
        plt.ylabel("Nombre d'itérations", fontsize=14)

        # subs=range(2,150)
        subs = list(OrderedDict.fromkeys([int(i)+1 for i in np.logspace(0, 2.5, num=100)]))
        nbIt=np.zeros(len(subs))
        for w in range(10,101,40):
            self.omega = w
            print(".")
            for i in range(len(subs)):
                Nsub = subs[i]            
                self.N = Nsub+1

                Ztemp = [\
                1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                    [-1.0, np.cosh(self.omega/Nsub)]])\
                ]

                # Ztemp = [np.array([\
                # [1, 0.0],\
                # [0.0, 1]\
                # ])]

                self.Z = Ztemp*Nsub

                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solveFuncErr(100000, 1e-4)
                nbIt[i]=it

            lines+=plt.plot([i for i in subs], nbIt)

        matplotlib.rcParams.update({'font.size': 10})
        legend1 = ax.legend(lines[:], ['w=10', 'w=50', 'w=90'],
          loc='upper right', bbox_to_anchor=(1.1, 1.05))
        ax.legend(lines2[:], ['w=10', 'w=50', 'w=90'],
          loc='lower right', bbox_to_anchor=(1.1, -0.05))
        plt.gca().add_artist(legend1)
        plt.show()

    #Même chose avec comparaison des deux produits scalaires
    def plotIterationsBeforeConvergenceByNBSubDomainsZIdAndDtN(self):
        self.reset()

        matplotlib.rcParams.update({'font.size': 14})

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        lines = ax.plot()
        lines2 = lines.copy()
        ax.grid()        
        plt.xlabel("Nombre de sous-domaines", fontsize=14)
        plt.ylabel("Nombre d'itérations", fontsize=14)

        subs=range(3,300)
        nbItId=np.zeros(len(subs))
        nbItDtN=np.zeros(len(subs))
        for w in range(10,101,20):
            self.omega = w
            for i in range(len(subs)):
                Nsub = subs[i]            
                self.N = Nsub

                Ztemp = [\
                1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                    [-1.0, np.cosh(self.omega/Nsub)]])\
                ]
                
                self.Z = Ztemp*Nsub
                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solve(1000, 1e-4)
                nbItDtN[i]=it

                Ztemp = [np.array([\
                [1.0, 0.0],\
                [0.0, 1.0]\
                ])]
                self.Z = Ztemp*Nsub
                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solve(1000, 1e-4)
                nbItId[i]=it

            lines+=plt.plot(subs, nbItDtN/nbItId)

        matplotlib.rcParams.update({'font.size': 10})
        legend1 = ax.legend(lines[:], ['w=10', 'w=30', 'w=50', 'w=70', 'w=90'],
          loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.show()

    #Même chose mais on fait varier le nombre de sous-domaines en gardant h = cst (on agrandit le domaine)
    def plotIterationsBeforeConvergenceByNBSubDomainsHCst(self):
        self.reset()

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        ax.plot()
        ax.grid()  
        plt.xlabel("Nombre de sous-domaines")
        plt.ylabel("Nombre d'itérations")

        subs=range(3,300)
        nbIt=np.zeros(len(subs))
        for i in range(len(subs)):
            Nsub = subs[i]            
            self.N = Nsub
            Ztemp =[\
            1/np.sinh(self.omega/Nsub)*np.array([[np.cosh(self.omega/Nsub), -1.0],\
                                [-1.0, np.cosh(self.omega/Nsub)]])\
            ]
            self.Z = Ztemp*Nsub
            self.b = 0.1*(self.N-1)
            self.updateSolver()
            trans, it, err = self.analyticJDDsolver.solve(1000, 1e-4)
            nbIt[i]=it

        plt.scatter(subs, nbIt)

        plt.show()

    #Cette fois en fonction de la fréquence
    def plotIterationsBeforeConvergenceByFq(self):
        self.reset()

        matplotlib.rcParams.update({'font.size': 14})

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        lines = ax.plot()
        ax.grid()        
        plt.xlabel("Pulsation ($\omega$)", fontsize=14)
        plt.ylabel("Nombre d'itérations", fontsize=14)

        freq=range(5,1000,10)
        nbIt=np.zeros(len(freq))

        for N in range(5,51,20):
            self.N=2*N+1      
            for i in range(len(freq)):
                fq = freq[i]
                self.omega=fq
                # Ztemp = [\
                # 1/np.sinh(self.omega/(self.N-1))*np.array([[np.cosh(self.omega/(self.N-1)), -1.0],\
                #                     [-1.0, np.cosh(self.omega/(self.N-1))]])\
                # ]
                Ztemp = [np.array([\
                [2, 0.0],\
                [0.0, 2]\
                ])]
                self.Z = Ztemp*(self.N-1)
                # self.Z[0][0][0]=1
                # self.Z[-1][1][1]=1
                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solve(100000, 1e-4)
                nbIt[i]=it

            lines += plt.plot(freq, nbIt, label='$J = %i$' % (self.N-1))

            max_fq=max(freq)
            max_it=max(nbIt)
            quotient = int(max_fq/self.analyticJDDsolver.h*np.pi)

        matplotlib.rcParams.update({'font.size': 10})
        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.show()

    #analogue à celle par sous-domaines, sauf que par fréquence ici
    def plotIterationsBeforeConvergenceFuncByFq(self):
        self.reset()

        matplotlib.rcParams.update({'font.size': 14})

        figure, ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.45)
        lines = ax.plot()
        ax.grid()        
        plt.xlabel("Pulsation ($\omega$)", fontsize=14)
        plt.ylabel("Nombre d'itérations", fontsize=14)

        freq=range(5,1000,10)
        nbIt=np.zeros(len(freq))

        for N in range(5,51,20):
            self.N=2*N+1      
            for i in range(len(freq)):
                fq = freq[i]
                self.omega=fq
                Ztemp = [\
                1/np.sinh(self.omega/(self.N-1))*np.array([[np.cosh(self.omega/(self.N-1)), -1.0],\
                                    [-1.0, np.cosh(self.omega/(self.N-1))]])\
                ]
                # Ztemp = [np.array([\
                # [1, 0.0],\
                # [0.0, 1]\
                # ])]
                self.Z = Ztemp*(self.N-1)
                # self.Z[0][0][0]=1
                # self.Z[-1][1][1]=1
                self.updateSolver()
                trans, it, err = self.analyticJDDsolver.solveFuncErr(100000, 1e-4)
                nbIt[i]=it

            lines += plt.plot(freq, nbIt, label='$J = %i$' % (self.N-1))

            max_fq=max(freq)
            max_it=max(nbIt)
            quotient = int(max_fq/self.analyticJDDsolver.h*np.pi)

        matplotlib.rcParams.update({'font.size': 10})
        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.05))
        plt.show()