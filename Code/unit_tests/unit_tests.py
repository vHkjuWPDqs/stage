

from helmholtz.finite_element_solvers.p2_solver import P2_solver 

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

#permet simplement de faire des tests afin de valider le code

class EF_P2_TESTS:

    @staticmethod
    def test(N_FE, Z, display):
        #pulsation
        omega=10

        #intervalle ]a,b[
        a=0.0
        b=1

        #f : second membre
        f=lambda x : 0.0

        # g : fct bord
        g=[0.0, omega*2j*np.exp(omega*1j*b)]

        #Pour vérifier analytiquement
        matsol=np.zeros((2, 2), dtype=complex)

        matsol[0][0]=np.exp(-1j*omega*a)
        matsol[0][1]=np.exp(1j*omega*a)
        matsol[1][0]=np.exp(-1j*omega*b)
        matsol[1][1]=np.exp(1j*omega*b)

        matsol = Z.dot(matsol)

        matsol[0][0]+=np.exp(-1j*omega*a)
        matsol[0][1]-=np.exp(1j*omega*a)
        matsol[1][0]-=np.exp(-1j*omega*b)
        matsol[1][1]+=np.exp(1j*omega*b)

        matsol = 1j*omega*matsol

        matsol = np.linalg.inv(matsol)

        AB = matsol.dot(g)

        #solver EF
        finiteElementSolver = P2_solver(omega, f, g, a, b, N_FE, Z)
        c = finiteElementSolver.solve()


        #solution analytique pour les coeffs EF
        c_sol = np.zeros(len(c), dtype=complex)

        for i in range(len(c)):
            c_sol[i]=AB[0]*np.exp(-1j*omega*finiteElementSolver.x[i])+AB[1]*np.exp(1j*omega*finiteElementSolver.x[i])

        #Erreur brute
        err = c-c_sol
        
        if display:        

            x=np.linspace(a, b, 10000)
            y = np.zeros(len(x), dtype=complex)
            y_sol = np.zeros(len(x), dtype=complex)#solution analytique A*exp(-iwx)+B*exp(iwx) avec [A, B]=AB

            for i in range(len(x)):
                y[i]=finiteElementSolver(x[i])
                y_sol[i]=AB[0]*np.exp(-1j*omega*x[i])+AB[1]*np.exp(1j*omega*x[i])               

            #Affichage / Visualisation
            fig = plt.figure()
            ax = Axes3D(fig)
            ax.scatter(x, y.real, y.imag, s=0.5)
            ax.scatter(x, y_sol.real, y_sol.imag, s=0.5)
            ax.set_xlabel('X-axis')
            ax.set_ylabel('Y-axis')
            ax.set_zlabel('Z-axis')
            plt.show()
        return np.abs(np.vdot(err, err)/np.vdot(c_sol, c_sol))

    @staticmethod
    def test_Z_id(display=False):
        Z=np.array([[1.0, 0.0], [0.0, 1.0]])
        #Affichage erreur relative
        print("Erreur EF au carré pour Z = Id: %f" %(EF_P2_TESTS.test(10, Z, display)))
        

    @staticmethod
    def test_Z_diag(display=False):
        Z=np.array([[2.0, 0.0], [0.0, 3.0]])
        print("Erreur EF au carré pour Z diagonale: %f" %(EF_P2_TESTS.test(10, Z, display)))

    @staticmethod
    def test_Z_full(display=False):
        Z=np.array([[2.0, 4.0], [4.0, 3.0]])
        print("Erreur EF au carré pour Z pleine: %f" %(EF_P2_TESTS.test(10, Z, display)))

from helmholtz.domain_decomposition_solvers.iterative_solvers import AnalyticJacobiDDMSolver
from scipy.linalg import eig



class DDM_TESTS:

    def __init__(self, N, Z):
        self.Zsauv = Z
        self.Z=Z
        self.omega = 10
        self.a = 0
        self.b = 1
        self.N = N
        self.g = [-self.omega*2j*np.exp(self.omega*1j*self.a), 0.0]

        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)
    
    def updateSolver(self):
        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)

    def reset(self):
        self.Z = self.Zsauv
        self.omega = 10
        self.a = 0
        self.b = 1
        self.N = 10
        self.g = [-self.omega*2j*np.exp(self.omega*1j*self.a), 0.0]

        self.analyticJDDsolver = AnalyticJacobiDDMSolver(self.omega, self.g, self.a, self.b, self.N, self.Z)

    def checkPiSymmetry(self):
        self.reset()
        ZPi = self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.Pi)
        [DPI, PPI] = eig(ZPi, self.analyticJDDsolver.ZMat)
        flag = False
        for i in range(len(DPI)):
            if not (1-1e-6 < DPI[i] < 1+1e-6 or -1-1e-6 < DPI[i] < -1+1e-6):
                flag = True
                break
        if flag:
            print("ERROR (Pi eigenvalues) : ")
            print(DPI)
        else:
            print("Pi Symmetry (eigenvalues) : __OK__")

    def checkPiPlusIdentityOver2IsProj(self):
        self.reset()
        P = self.analyticJDDsolver.Pi.copy()
        for i in range(len(P)):
            P[i][i]+=1.0
        P = 0.5*P

        i=0
        b = True
        res=np.random.random(len(P))
        while i < 100 and b:
            random_vect = np.random.random(len(P))
            res = P.dot(random_vect)
            j = 0
            bb = True
            while j < len(P)//2-1 and bb:
                err = abs(res[2*j+1]-res[2*j+2])
                if  err > 1e-6:
                    bb = False
                    b = False
                else:
                    j+=1
            if b:
                i+=1
        if not b:
            print("ERROR (Pi+I)/2 is not a projection on single trace subspace")
            print(res)
        else:
            print("(Pi+I)/2 proj : __OK__")

    def checkSContraction(self):
        self.reset()

        ZS = self.analyticJDDsolver.ZMat.dot(self.analyticJDDsolver.S)
        [DS, PS] = eig(ZS, self.analyticJDDsolver.ZMat)

        flag = False
        for eigenValue in DS:
            if not (abs(eigenValue) <= 1+1e-10):
                flag = True
                break
        if flag:
            print("ERROR (S eigenvalues) : ")
            print(abs(DS))
        else:
            print("S Contraction : __OK__")

    def checkpPlusPiSpEQf(self):
        self.reset()
        err = np.linalg.norm(self.analyticJDDsolver.trans + self.analyticJDDsolver.Pi.dot(self.analyticJDDsolver.S.dot(self.analyticJDDsolver.trans) + self.analyticJDDsolver.f))\
            /np.linalg.norm(self.analyticJDDsolver.Pi.dot(self.analyticJDDsolver.f))

        if err > 1e-6:
            print("ERROR p + Pi(S(p)) != f: ")
            print(err)
        else:
            print("p + Pi(S(p)) = f : __OK__")

    def checkSpEQpPlus2iw(self):
        self.reset()

        self.analyticJDDsolver.solve(10000)

        p = self.analyticJDDsolver.trans.copy()

        u = self.analyticJDDsolver.getUBoundaries()

        u_etoile = -self.analyticJDDsolver.f * 0.5j

        err = np.linalg.norm(self.analyticJDDsolver.S.dot(p)-p-2j*(u-u_etoile))

        if err > 1e-6:
            print("ERROR Sp != p + 2i(u-u*) : ")
            print(err)
        else:
            print("Sp = p + 2i(u-u*) : __OK__")
    
    def checkSpEQpPlus2iwAnalytic(self):
        self.reset()
        self.Z = [np.array([[self.omega, 0],[0, self.omega]])]*(self.N-1)
        self.updateSolver()

        i=0
        b=True

        while i < 100 and b:
            p = np.random.random(2*(self.N-1))+1j*np.random.random(2*(self.N-1))

            w = np.zeros(2*(self.N-1), dtype=complex)

            for k in range(self.N-1):

                temp=np.zeros((2, 2), dtype=complex)
                wh = self.analyticJDDsolver.omega*self.analyticJDDsolver.h
                if k == 0:
                    temp[0][0]=np.cos(wh)-1j*np.sin(wh)
                    temp[0][1]=1
                    temp[1][0]=1
                    temp[1][1]=np.cos(wh)-2j*np.sin(wh)
                    
                    temp *= np.sin(wh)/self.analyticJDDsolver.omega/((np.cos(wh)-1j*np.sin(wh))*(np.cos(wh)-2j*np.sin(wh))-1)
                elif k == self.N-2:
                    temp[0][0]=np.cos(wh)-2j*np.sin(wh)
                    temp[0][1]=1
                    temp[1][0]=1
                    temp[1][1]=np.cos(wh)-1j*np.sin(wh)

                    temp *= np.sin(wh)/self.analyticJDDsolver.omega/((np.cos(wh)-1j*np.sin(wh))*(np.cos(wh)-2j*np.sin(wh))-1)
                else:
                    temp[0][0]=np.cos(wh)-1j*np.sin(wh)
                    temp[0][1]=1
                    temp[1][0]=1
                    temp[1][1]=np.cos(wh)-1j*np.sin(wh)

                    temp *= np.sin(wh)/self.analyticJDDsolver.omega/((np.cos(wh)-1j*np.sin(wh))**2-1)
                
                tempinv = temp.copy()

                temp *= self.analyticJDDsolver.omega
                temp  *= 2j

                temp2 = temp.dot(np.array([p[2*k], p[2*k+1]]))
                
                w[2*k]=temp2[0]
                w[2*k+1]=temp2[1]
                
            w += p

            err = np.linalg.norm(w-self.analyticJDDsolver.S.dot(p))/np.linalg.norm(w)
            if err > 1e-6:
                b=False
                print("ERROR Sp != p + 2iw : ")
                print(err)
            else:
                i+=1
        if b:
            print("Sp = p + 2iw : __OK__")