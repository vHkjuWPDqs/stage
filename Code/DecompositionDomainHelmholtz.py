import numpy as np
from scipy.linalg import solve
import scipy

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

################################################################## Éléments finis ##################################################################################
from unit_tests.unit_tests import EF_P2_TESTS

EF_P2_TESTS.test_Z_id()
EF_P2_TESTS.test_Z_diag()
EF_P2_TESTS.test_Z_full()

################################################################## Décomposition de domaines #######################################################################



#pulsation
omega=10

#intervalle ]a,b[
a=0.0
b=1

#f : second membre
f=lambda x : 0.0

# g : fct bord
g=[-omega*2j*np.exp(omega*1j*a), 0.0]

################# Analytique ################
from unit_tests.unit_tests import DDM_TESTS
from plots.plots import PLOTS

N=101

# Z=[np.array([[1.0,0.0],[0.0,1.0]])]*(N-1)
# Z=[np.array([[10.0,0.0],[0.0,10.0]])]*(N-1)
# Z=[np.array([[30.0,-8.0],[-8.0,40.0]])]*(N-1)

Z=[\
    1/np.sinh(omega/(N-1))*np.array([[np.cosh(omega/(N-1)), -1.0],\
                         [-1.0, np.cosh(omega/(N-1))]])\
    ]*(N-1)


# Z=[\
#     1/np.sinh((b-a)/(N-1))*np.array([[np.cosh((b-a)/(N-1)), -1.0],\
#                          [-1.0, np.cosh((b-a)/(N-1))]])\
#     ]*(N-1)

dm = DDM_TESTS(N, Z)
# dm.checkPiSymmetry()
# dm.checkSContraction()
# dm.checkSpEQpPlus2iw()
# dm.checkpPlusPiSpEQf()
# dm.checkPiPlusIdentityOver2IsProj()
# dm.checkSpEQpPlus2iwAnalytic()

plot = PLOTS(N, Z)
# plot.plotSEigenValues()
#plot.plotAEigenValues()
# plot.plotAEigenValuesIdDtN()
# plot.plotConvergence()
# plot.plotErrorGraph()
# plot.plotErrorGraphIdAndDtN()
# plot.plotErrorGraphFuncIdAndDtN()
# plot.plotInfSupNeumannByFq()
# plot.plotGammaDeteriorationIdAndDtN()
plot.plotGammaDeteriorationAndTheoricalDeterioration()
# plot.plotIterationsBeforeConvergenceByNBSubDomainsHCst()
# plot.plotSVDIdAndDtN()
# plot.plotIterationsBeforeConvergenceByNBSubDomains()
# plot.plotIterationsBeforeConvergenceFuncByNBSubDomains()
# plot.plotIterationsBeforeConvergenceByNBSubDomainsZIdAndDtN()
# plot.plotIterationsBeforeConvergenceByFq()
# plot.plotIterationsBeforeConvergenceFuncByFq()

################ Avec EF ####################
from helmholtz.domain_decomposition_solvers.iterative_solvers import JacobiDDMSolver

# finiteElementJacobiSolver = JacobiDDMSolver(omega, g, 0, 1, 10, Z, 10)
# finiteElementJacobiSolver.solve()

# x = np.linspace(a, b, 1000)
# y = np.array([finiteElementJacobiSolver(xi) for xi in x], dtype=complex)

# fig = plt.figure()
# ax = Axes3D(fig)
# ax.scatter(x, y.real, y.imag, s=0.5)
# ax.set_xlabel('X-axis')
# ax.set_ylabel('Y-axis')
# ax.set_zlabel('Z-axis')

# y = np.array([np.exp(1j*omega*xi) for xi in x], dtype=complex)
# ax.scatter(x, y.real, y.imag, s=0.5)
# plt.show()

# print(finiteElementJacobiSolver.trans)