from numpy import exp, cos, sin, sqrt, real, conj
from numpy import linspace
from numpy import array, zeros, eye, vdot
from numpy import append

from numpy.linalg import inv, norm
from scipy.sparse.linalg import spsolve

from scipy.sparse import csr_matrix
from scipy.linalg import cholesky

from matplotlib.pyplot import figure, title, plot, show
from mpl_toolkits.mplot3d import Axes3D

from ..finite_element_solvers.p2_solver import P2_solver 
from ..finite_element_solvers.p2_solver import DIRECTION


class AnalyticJacobiDDMSolver:

    def __init__(self, omega, g, a, b, N, Z):
        """[summary]

        Args:
            omega (float): [pulsation]
            g (array 2x1): [conditions de bord]
            a (float): [borne gauche de l'intervalle]
            b (float): [borne droite de l'intervalle]
            N (int): [nombre de points pour la décomposition de domaines]
            Z (array of matrices): [impédances (sans omega)]
        """

        self.omega = omega
        self.N=N
        self.x=linspace(a,b,self.N)
        
        #vecteurs de traces
        self.trans=zeros(2 * (self.N-1), dtype=complex)
        
        self.g=g
        
        #multiplie par omega pour "normaliser"
        self.Z=[self.omega*Zi for Zi in Z]
        
        #Largeur d'un sous-domaine
        self.h = self.x[1]-self.x[0]

        #Z sous forme de matrice
        self.ZMat = zeros((2 * (self.N-1), 2 * (self.N-1)), dtype=complex)

        for i in range(self.N-1):
            self.ZMat[2*i][2*i]=self.Z[i][0][0]
            self.ZMat[2*i][2*i+1]=self.Z[i][0][1]
            self.ZMat[2*i+1][2*i]=self.Z[i][1][0]
            self.ZMat[2*i+1][2*i+1]=self.Z[i][1][1]

        ############################################  Dirichlet to Neumann  ################################

        self.Tloc = zeros((2,2), dtype=complex)
        self.Tloc[0][0] = cos(self.omega*self.h)
        self.Tloc[0][1] = -1
        self.Tloc[1][0] = -1
        self.Tloc[1][1] = cos(self.omega*self.h)
        self.Tloc[:][:] *= self.omega/sin(self.omega*self.h)
        
        ###########################################  Opérateur de scattering  ##############################

        self.S = zeros((2 * (self.N-1), 2 * (self.N-1)), dtype=complex)
        for i in range(self.N-1):
            # Sloc = (-1j*self.omega*self.Z[i]+self.Tloc).dot(inv(self.Tloc+1j*self.omega*self.Z[i]))

            Sloc = self.Tloc-1j*self.Z[i]

            if i == 0:
                Sloc[0][0]-=1j*self.omega
            elif i == self.N-2:
                Sloc[1][1]-=1j*self.omega

            Sloc = inv(Sloc)
            Sloc *= 2j
            Sloc = Sloc.dot(self.Z[i])

            Sloc[0][0]+=1
            Sloc[1][1]+=1

            self.S[2*i][2*i]    =Sloc[0][0]
            self.S[2*i][2*i+1]  =Sloc[0][1]
            self.S[2*i+1][2*i]  =Sloc[1][0]
            self.S[2*i+1][2*i+1]=Sloc[1][1]
        
        ###########################################  Opérateur d'échange  ##################################

        self.Pi = zeros((2 * (self.N-1), 2 * (self.N-1)), dtype=complex)
        self.R = zeros((2 * (self.N-1), self.N), dtype=complex)
        for i in range(self.N-1):
            self.R[2*i][i]=1.0
            self.R[2*i+1][i+1]=1.0

        #Pi = 2 R.(Rt.Z.R)^-1.Rt.Z-I
        self.Pi = self.R.T.dot(self.ZMat.dot(self.R))   #Rt.Z.R
        self.Pi = inv(self.Pi)                          #(Rt.Z.R)^-1
        self.Pi = self.R.dot(self.Pi.dot(self.R.T))     #R.(Rt.Z.R)^-1.Rt
        self.Pi = self.Pi.dot(self.ZMat)                #R.(Rt.Z.R)^-1.Rt.Z
        self.Pi = 2*self.Pi                             #2 R.(Rt.Z.R)^-1.Rt.Z
        for i in range(2*(self.N-1)):                   #2 R.(Rt.Z.R)^-1.Rt.Z-I
             self.Pi[i][i] -= 1.0

        ############################## I + Pi o S ################################

        self.A = self.Pi.dot(self.S)
        for i in range(len(self.A)):
            self.A[i][i]+=1.0
    
        ############################## Second membre #############################
        self.f = zeros(len(self.trans), dtype=complex)

        #calcul de f
        temp1 = self.Tloc -1j*self.Z[0]
        temp1[0][0]-=1j*self.omega
        temp2 = self.Tloc - 1j*self.Z[-1]
        temp2[1][1]-=1j*self.omega
        f1 = inv(temp1).dot(array([self.g[0], 0]))
        f2 = inv(temp2).dot(array([0, self.g[1]]))

        self.f[0]=f1[0]
        self.f[1]=f1[1]
        self.f[-2]=f2[0]
        self.f[-1]=f2[1]

        self.f = 2j*self.f

        ########################### Trace analytique ############################

        self.analytic_trans = zeros(2 * (self.N-1), dtype=complex)

        self.AB_theorique = array([g[1]/2j/self.omega*exp(1j*self.omega*self.x[-1]), -g[0]/2j/self.omega*exp(-1j*self.omega*self.x[0])])

        # inv_matrix=inv(1j*self.omega*array([[exp(-1j*self.omega*self.x[0]),-exp(-1j*self.omega*self.x[0])],\
        #                 [-exp(-1j*self.omega*self.x[-1]),exp(-1j*self.omega*self.x[-1])]]))
        # AB_theorique=inv_matrix.dot(array(self.g))
        
        for k in range(self.N-1):

            #matrice (u(a), u(b))=N*(A, B)
            Nk = zeros((2,2), dtype=complex)
            Nk[0][0]=exp(-1j*self.omega*self.x[k])
            Nk[0][1]=exp(1j*self.omega*self.x[k])
            Nk[1][0]=exp(-1j*self.omega*self.x[k+1])
            Nk[1][1]=exp(1j*self.omega*self.x[k+1])
            temp = self.Tloc-1j*self.Z[k]
            
            #vecteur qui représente g dans l'espace multitrace
            hk = zeros(2, dtype=complex)
            if k == 0:
                temp[0][0]-=self.omega*1j
                hk[0]=self.g[0]
            elif k == self.N-2:
                temp[1][1]-=self.omega*1j
                hk[1]=self.g[1]

            #calcul pour retrouver pk pk+1
            temp = temp.dot(Nk)           

            temp2 = temp.dot(self.AB_theorique)-hk
            temp2 = inv(self.Z[k]).dot(temp2)
            self.analytic_trans[2*k] = temp2[0]
            self.analytic_trans[2*k+1] = temp2[1]

    def getH1NormDiff(self):
        w=self.omega

        normL2sqDiff=0.0
        normDL2sqDiff=0.0
        normL2sq=0.0
        normDL2sq=0.0

        AB=self.getAB()
        for j in range(self.N-1):
            #Diff
            A=AB[j][0]-self.AB_theorique[0]
            B=AB[j][1]-self.AB_theorique[1]

            AsqPBsq=abs(A)**2+abs(B)**2
            ILength = self.h
            expDiff = exp(-2j*w*self.x[j+1])-exp(-2j*w*self.x[j])

            #se simplifie mais peut être utile par la suite ?
            normL2sqDiff+=AsqPBsq*ILength+2*real(A*conj(B)/w*expDiff)
            normDL2sqDiff+=w**2*AsqPBsq*ILength-2*real(A*conj(B)/w*expDiff)

            #Denominateur
            A=self.AB_theorique[0]
            B=self.AB_theorique[1]

            AsqPBsq=abs(A)**2+abs(B)**2

            normL2sq+=AsqPBsq*ILength+2*real(A*conj(B)/w*expDiff)
            normDL2sq+=w**2*AsqPBsq*ILength-2*real(A*conj(B)/w*expDiff)
        return sqrt(self.omega**2*normL2sqDiff+normDL2sqDiff)/sqrt(self.omega**2*normL2sq+normDL2sq)
    
    def solve(self, N_it = 100, eps = 1e-6):
        """Résolution par Jacobi
        """
        r=1.0/sqrt(2)

        it = 0
        err = eps + 1
        while it < N_it and err > eps:
            self.trans = (1-r)*self.trans - r*self.Pi.dot(self.S.dot(self.trans)+self.f)
            err = norm(self.trans-self.analytic_trans)/norm(self.analytic_trans)
            it += 1

        return [self.trans, it, err]

    def solveFuncErr(self, N_it = 100, eps = 1e-6):
        """Résolution par Jacobi
        """
        r=1.0/sqrt(2)

        it = 0
        err = eps + 1
        while it < N_it and err > eps:
            self.trans = (1-r)*self.trans - r*self.Pi.dot(self.S.dot(self.trans)+self.f)
            err = self.getH1NormDiff()
            it += 1

        return [self.trans, it, err]

    def getAB(self):#Ae^{-iwx}+Be^{iwx}
        Nsub=self.N-1
        AB=zeros((Nsub,2), dtype=complex)
        for j in range(Nsub):
            temp_mat=self.Tloc-1j*self.Z[j]
            h=zeros(2, dtype=complex)
            if j == 0:
                temp_mat[0][0]-=1j*self.omega
                h=array([self.g[0], 0.0], dtype=complex)
            elif j == Nsub-1:
                temp_mat[-1][-1]-=1j*self.omega
                h=array([0.0, self.g[1]], dtype=complex)

            ploc=array([self.trans[2*j], self.trans[2*j+1]], dtype=complex)
            uaub=inv(temp_mat).dot(self.Z[j].dot(ploc)+h)

            N = array([[exp(-1j*self.omega*self.x[j]),exp(1j*self.omega*self.x[j])],\
                [exp(-1j*self.omega*self.x[j+1]),exp(1j*self.omega*self.x[j+1])]], dtype=complex)
            AB[j]=inv(N).dot(uaub)
        return AB

    def iterate(self):
        """Résolution par Jacobi
        """
        r=1.0/sqrt(2)
        self.trans = (1-r)*self.trans - r*self.Pi.dot(self.S.dot(self.trans)+self.f)

    def directSolve(self):
        self.trans = spsolve(self.A, -self.Pi.dot(self.f))

    def __getSubDomain(self, value):
        """Retourne l'indice du x[k] le plus proche en valeur inférieure

        Args:
            value (float): [la variable]

        Returns:
            int: [indice du x[k] le plus proche]
        """
        return int((value-self.x[0])//(self.h))

    def __call__(self, value):
        k=self.__getSubDomain(value)
        if k >= self.N - 1:
            k -= 1
        
        Nk = zeros((2,2), dtype=complex)
        Nk[0][0]=exp(-1j*self.omega*self.x[k])
        Nk[0][1]=exp(1j*self.omega*self.x[k])
        Nk[1][0]=exp(-1j*self.omega*self.x[k+1])
        Nk[1][1]=exp(1j*self.omega*self.x[k+1])
        temp = self.Tloc-1j*self.Z[k]

        hk = zeros(2, dtype=complex)
        if k == 0:
            temp[0][0]-=self.omega*1j
            hk[0]=self.g[0]
        elif k == self.N-2:
            temp[1][1]-=self.omega*1j
            hk[1]=self.g[1]

        temp = temp.dot(Nk)
        temp = inv(temp)
        p = array([self.trans[2*k], self.trans[2*k+1]])
            
        AB = temp.dot(self.Z[k].dot(p)+hk)

        return AB[0]*exp(-1j*self.omega*value)+AB[1]*exp(1j*self.omega*value)

    def getUBoundaries(self):
        u = zeros(2*(self.N-1), dtype=complex)
        for k in range(self.N-1):
            temp = self.Tloc-1j*self.Z[k]

            hk = zeros(2, dtype=complex)
            if k == 0:
                temp[0][0]-=self.omega*1j
                hk[0]=self.g[0]
            elif k == self.N-2:
                temp[1][1]-=self.omega*1j
                hk[1]=self.g[1]

            temp = inv(temp)
            p = array([self.trans[2*k], self.trans[2*k+1]])
                
            utemp = temp.dot(self.Z[k].dot(p)+hk)

            u[2*k]=utemp[0]
            u[2*k+1]=utemp[1]
        return u
    def postProcess(self, transExpected):
        """Calculs après résolution

        Args:
            transExpected (function): [traces espérées].

        Returns:
            [float]: [erreur]
        """
        err = 0
        normExpected = 0
        for k in range(self.N-1):
            err += abs(self.trans[2*k]-transExpected(self.x[k], -1))**2
            err += abs(self.trans[2*k+1]-transExpected(self.x[k+1], 1))**2
            normExpected += abs(transExpected(self.x[k], -1))**2 + abs(transExpected(self.x[k+1], 1))**2
        if normExpected != 0:#sinon la notion d'erreur relative n'a pas grand sens
            err/=normExpected
        return sqrt(err)

class JacobiDDMSolver:

    def __init__(self, omega, g, a, b, N, Z, N2):
        """Constructeur

        Args:
            omega (float): [pulsation]
            g (array 2x1): [conditions de bord]
            a (float): [borne gauche de l'intervalle]
            b (float): [borne droite de l'intervalle]
            N (int): [nombre de points pour la décomposition de domaines]
            N2 (array): [demi-nombres de points pour les EF sur chaque intervalle -> 2*N2[i]+1 points pour l'intervalle i]
        """
        if(isinstance(N2, int)):
            N2 = [N2 for i in range(N-1)]
        elif(len(N2) != N-1):
            lN2 = len(N2)
            print("N2 has incorrect size ! Has %d, expected %d" % (lN2, N-1))
            if(lN2 > N-1):#Si trop d'éléments, on tronque
                N2 = N2[:N-1]
            else:#Sinon on rajoute une valeur par défaut
                N2.extend([N2[-1] for i in range(N-1-lN2)])
        
        self.omega = omega
        self.N=N
        self.x=linspace(a,b,self.N)
        self.N2=N2
        
        #vecteurs de traces
        self.trans=zeros(2 * (self.N-1), dtype=complex)
        
        self.g=g
        
        self.trans[0]=self.g[0]
        self.trans[-1]=self.g[1]    

        self.Z = Z

        #Largeur d'un sous-domaine
        self.h = self.x[1]-self.x[0]
        
        #Matrice pour passer des traces sortantes à entrantes
        self.Pi = zeros((2 * (self.N-1), 2 * (self.N-1)), dtype=complex)
        
        temp_inv = zeros((self.N, self.N))

        for i in range(self.N-1):
            temp_inv[i][i]     +=self.Z[i][0][0]
            temp_inv[i][i+1]   +=self.Z[i][0][1]
            temp_inv[i+1][i]   +=self.Z[i][1][0]
            temp_inv[i+1][i+1] +=self.Z[i][1][1]

        temp_inv = inv(temp_inv)

        #Pi = 2P-I
        for i in range(self.N-1):
            for j in range(self.N-1):
                self.Pi[2*i][2*j]=temp_inv[i][j]
                self.Pi[2*i][2*j+1]=temp_inv[i][j+1]
                self.Pi[2*i+1][2*j]=temp_inv[i+1][j]
                self.Pi[2*i+1][2*j+1]=temp_inv[i+1][j+1]

        for i in range(self.N-1):
            for j in range(self.N-1):
                temp_mult = zeros((2, 2), dtype=complex)

                temp_mult[0][0] = self.Pi[2*i,2*j]
                temp_mult[0][1] = self.Pi[2*i,2*j+1]
                temp_mult[1][0] = self.Pi[2*i+1,2*j]
                temp_mult[1][1] = self.Pi[2*i+1,2*j+1]

                temp_mult = temp_mult.dot(Z[j])

                self.Pi[2*i,2*j]        = 2*temp_mult[0][0]
                self.Pi[2*i,2*j+1]      = 2*temp_mult[0][1]
                self.Pi[2*i+1,2*j]      = 2*temp_mult[1][0]
                self.Pi[2*i+1,2*j+1]    = 2*temp_mult[1][1]

        for i in range(2*(self.N-1)):
            self.Pi[i][i] -= 1.0
        
        #Solvers EF sur chaque sous-domaine
        self.solvers=array([P2_solver(self.omega, lambda x : 0.0, [self.trans[2*k], self.trans[2*k+1]], self.x[k], self.x[k+1], self.N2[k], Z[k]) for k in range(self.N-1)], dtype=object)
        
    def exchange(self):
        """Opérateur d'échange
        """
        self.trans = self.Pi.dot(self.trans)

    def applyEFScattering(self):
        """Opérateur de scattering
        """

        for k in range(self.N-1):#Résolution sur chaque sous-domaine
            self.solvers[k].c=self.solvers[k].solve()

        # for i in range(1, len(self.trans)-1):
        #     self.trans[i] = 2j*self.omega*self.solvers[int(i/2)].c[-1 if i & 0b1 else 0] - self.trans[i]


        for i in range(self.N-1):
            #anciennes traces
            v = zeros(2, dtype=complex)
            v[0]=self.trans[2*i]
            v[1]=self.trans[2*i+1]

            #solution calculée par EF
            u = zeros(2, dtype=complex)
            u[0]=self.solvers[i].c[0]
            u[1]=self.solvers[i].c[-1]

            v = 2j * self.omega * self.Z[i].dot(u) - v

            if i == 0:
                v[0]=self.trans[0]
            elif i == self.N-2:
                v[1]=self.trans[2*self.N-3]

            self.trans[2*i] = v[0]
            self.trans[2*i+1]= v[1]

    def updateBoundaryConditions(self):
        for k in range(len(self.solvers)):            
            self.solvers[k].setG([self.trans[2*k], self.trans[2*k+1]])

    def solve(self):
        """Résolution
        """
        for it in range(1,100):#itération
            self.updateBoundaryConditions()
            self.applyEFScattering()
            self.exchange()
    
    def __getSubDomain(self, value):
        """Retourne l'indice du x[k] le plus proche en valeur inférieure

        Args:
            value (float): [la variable]

        Returns:
            int: [indice du x[k] le plus proche]
        """
        return int((value-self.x[0])//(self.h))

    def __call__(self, value):
        """calcule u(value) où u est la solution préalablement calculée par @solve

        Args:
            value (float): [la variable]

        Returns:
            float: u(value)
        """
        sub = self.__getSubDomain(value)    
        if sub >= self.N - 1:
            sub -= 1
        return self.solvers[sub](value)

    def postProcess(self, uExpected):
        """Calculs d'erreur

        Args:
            uExpected (fonction): [description]

        Returns:
            [type]: [description]
        """
        err = 0
        rel = 0
        for k in range(self.N-1):
            v = uExpected(self.solvers[k].x)
            diff = self.solvers[k].c - v
            err += vdot(diff, diff)
            rel += vdot(v, v)
        return sqrt(err/rel)