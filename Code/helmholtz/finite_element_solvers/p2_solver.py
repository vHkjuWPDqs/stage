from enum import Enum
from numpy import exp, linspace, zeros, array
from scipy.sparse import diags
from scipy.linalg import inv
from scipy.sparse.linalg import spsolve

class DIRECTION(Enum):
    """[summary]
        Cette classe servira à savoir si l'on prend la "partie droite" ou la "partie gauche" des fonctions de forme car celles avec un indice pair s'étallent de x[k-2] à x[k+2]. 
        Donc gauche représente [x[k-2], x[k]] et droite [x[k], x[k+2]]
    """
    GAUCHE = 0
    DROITE = 1

class P2_solver:

    def __init__(self, omega=10.0, f=lambda x : 0.0, g=[0.0, 2*10.0*1j*exp(10.0*1j)], a=0, b=1, N=50, Z=array([[1.0, 0.0], [0.0, 1.0]])):
        """Constructeur de la classe
            
        Args:
            omega (float, optional): [pulsation]. Defaults to 10.
            f ([type], optional): [source]. Defaults to z.
            g (list, optional): [condition de bord]. Defaults to [0.0, 2*10.0*1j*exp(10.0*1j)].
            a (int, optional): [borne gauche de l'intervalle de calcul]. Defaults to 0.
            b (int, optional): [borne droite de l'intervalle de calcul]. Defaults to 1.
            N (int, optional): [demi nombre de points pour le maillage]. Defaults to 101.
        """
        if(N<2):
            print("Choose N >= 2")
        self.omega = omega
        self.f = f
        self.g = g
        self.N = 2*N+1
        self.x = linspace(a, b, self.N)
        self.h = self.x[1]-self.x[0]
        self.Z = Z
    
    def setG(self, g):
        """Mise à jour des conditions de bord

        Args:
            g (tableau 2x1): [nouvelles conditions de bord]
        """
        self.g = g

    def phi(self, k, direction, x):
        """Fonction de forme transformée par la fonction affine

        Args:
            k (int): [indice de la fonction]
            direction (DIRECTION): [gauche / droite]
            x (float): [variable]

        Returns:
            [float]: [phi_k(x)]
        """
        if (k > self.N-1 or k == self.N and direction == DIRECTION.DROITE):#permet une écriture simplifiée pour le calcul y=u(x) avec
            return 0
        elif(k & 0b1):                              #k impair -> phi2 o T(k)
            x=(x-self.x[k-1])/2/self.h              # |
            return 4*x*(1-x)                        # |
        else:                                     #k pair
            if(direction == DIRECTION.GAUCHE):      #phi3 o T(k-1)
                x=(x-self.x[k-2])/2/self.h            # |
                return x*(2*x-1)                      # |
            else:                                   #phi1 o T(k+1)
                x=(x-self.x[k])/2/self.h              # |
                return (2*x-1)*(x-1)                  # |
            
    def dphi(self, k, direction, x):
        if(k & 0b1):                              #k impair -> phi2 o T(k)
            x=(x-self.x[k-1])/2/self.h              # |
            return 4-8*x                            # |
        else:                                     #k pair
            if(direction == DIRECTION.GAUCHE):      #phi3 o T(k-1)
                x=(x-self.x[k-2])/2/self.h            # |
                return 4*x-1                          # |
            else:                                   #phi1 o T(k+1)
                x=(x-self.x[k])/2/self.h              # |
                return 4*x-3                          # |        
    
    def buildMatrix(self):
        """Assemble la matrice des éléments finis P2
        """
        
        #Les diagonales de K (matrice symétrique, 3 suffisent)
        K1=zeros(self.N-2)
        K2=zeros(self.N-1)
        K3=zeros(self.N)
        
        #Les diagonales de M (matrice symétrique, 3 suffisent)
        M1=zeros(self.N-2)
        M2=zeros(self.N-1)
        M3=zeros(self.N)
        
        
        ######################################### Construction de K ####################################
        for i in range((int)(self.N/2)):#1 sur 2
            K1[2*i]=1
        for i in range(self.N-1):
            K2[i]=-8
        for i in range(1,(int)(self.N/2)):#A partir du 3e, de 2 en 2 -> 14 et à partir du 4e, de 2 en 2 -> 16
            K3[2*i]=14
            K3[2*i+1]=16
            
        #Il reste ces trois cas non traités par la boucle précédente
        K3[0]=7
        K3[-1]=7
        K3[1]=16
        
        K=[K1, K2, K3, K2, K1]
        K=diags(K, [-2,-1, 0, 1, 2], dtype=complex)
        
        
        ######################################### Construction de M ####################################
        for i in range((int)(self.N/2)):#1 sur 2
            M1[2*i]=-1
        for i in range(self.N-1):
            M2[i]=2
        for i in range(1,(int)(self.N/2)):#A partir du 3e, de 2 en 2 -> 8 et à partir du 4e, de 2 en 2 -> 16
            M3[2*i]=8
            M3[2*i+1]=16
        #Il reste ces trois cas non traités par la boucle précédente
        M3[0]=4
        M3[-1]=4
        M3[1]=16
        M=[M1, M2, M3, M2, M1]
        M=diags(M, [-2,-1, 0, 1, 2], dtype=complex)
        
        
        ######################################### Assemblage de A avec K et M ####################################
        #   K <- K/6h    M <- M*h/15
        #A= K - w^2 M
        self.A=(1/6/self.h*K-self.omega*self.omega*self.h/15*M)       
     
        #+ iwZu aux extrémités 
        # self.A[0,0]+=complex(0,self.omega)
        # self.A[-1,-1]+=complex(0,self.omega)
        self.A[0,0]+= 1j * self.omega * self.Z[0][0]
        self.A[0,-1]+= 1j * self.omega * self.Z[0][1]
        self.A[-1,0]+= 1j * self.omega * self.Z[1][0]
        self.A[-1,-1]+= 1j * self.omega * self.Z[1][1]

    def integPhi(self, k, direction, res):
        """calcule l'intégrale du produit de f par une fonction de base phi_k. Pour rappel, phi_k s'étale sur [x_k-2, x_k+2],
        il nous faut donc connaitre l'intervalle de calcul [x_k-2, x_k] pour gauche et [x_k, x_k+2] pour droite (dans le cas k pair)

        Args:
            k (int): [indice de la fonction de base]
            direction (DIRECTION): [gauche : [x_k-2, x_k] ou droite : [x_k, x_k+2]]
            res (int): [résolution pour le calcul de l'intégrale]

        Returns:
            [float]: [intégrale de f contre phi_k]
        """
        a=0.0
        b=0.0
        if(k&0b1):          #k impair
            a=self.x[k-1]
            b=self.x[k+1]
        else:               #k pair
            if(direction == DIRECTION.GAUCHE):
                a=self.x[k-2]
                b=self.x[k]
            else:
                a=self.x[k]
                b=self.x[k+2]
                
        pas = (b-a)/res
        x=a
        s=0.0
        for i in range(res):
            s+=(self.f(x)*self.phi(k, direction, x)+self.f(x+pas)*self.phi(k, direction, x+pas))*pas/2.0
            x+=pas
        return s
    
    def buildVector(self):
        """Assemble le second membre des éléments finis P2
        """
        self.b = zeros(self.N, dtype=complex)
        
        #b=int(f, phi) (Écriture 1-2-3, puis 3-4-5, puis 5-6-7, etc.)
        for k in range((int)((self.N-1)/2)):#On parcourt les intervalles Ik de la forme [xk, xk+2]
            self.b[2*k]+=self.integPhi(2*k, DIRECTION.DROITE, 1000)
            self.b[2*k+1]=self.integPhi(2*k+1, DIRECTION.DROITE, 1000)
            self.b[2*k+2]=self.integPhi(2*k+2, DIRECTION.GAUCHE, 1000)
            
        #cdts de bord
        self.b[0]+=self.g[0]
        self.b[-1]+=self.g[1]

    def defaultLinearSolver(A, b):
        """Solveur linéaire utilisé par défaut pour résoudre les éléments finis

        Args:
            A (matrice): [matrice des éléments finis]
            b (vecteur): [second membre des éléments finis]

        Returns:
            [vecteur]: [solution du système linéaire]
        """
        return spsolve(A, b)
    
    def solve(self, linearSolver = defaultLinearSolver):
        """Applique la méthode des éléments finis dans sa globalité, assemblage + résolution

        Args:
            linearSolver (function, optional): [façon de résoudre le système linéaire]. Defaults to defaultLinearSolver.

        Returns:
            [vecteur]: [coefficients de la solution approchée dans Vh]
        """
        self.buildMatrix()
        self.buildVector()
        self.c = linearSolver(self.A, self.b)
        
        return self.c.copy()

    def __getIndex(self, value):
        """Retourne l'indice du x[k] le plus proche par valeur inférieure

        Args:
            value (float): [la variable]

        Returns:
            int: [indice du x[k] le plus proche]
        """
        return int((value-self.x[0])//(2*self.h))

    def __call__(self, value):
        """calcule u(value) où u est la solution préalablement calculée par @solve

        Args:
            value (float): [la variable]

        Returns:
            float: u(value)
        """
        k = self.__getIndex(value)
        if k < self.N // 2:
            return self.c[2*k]*self.phi(2*k, DIRECTION.DROITE, value) + \
                    self.c[2*k+1]*self.phi(2*k+1, DIRECTION.DROITE, value) + \
                    self.c[2*k+2]*self.phi(2*k+2, DIRECTION.GAUCHE, value) 
        else:
            return self.c[-1]*self.phi(self.N-1, DIRECTION.GAUCHE, value)