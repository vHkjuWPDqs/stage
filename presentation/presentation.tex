\documentclass{beamer}
\usetheme{Boadilla}

\usepackage{xcolor}
\usepackage{asymptote}
\usepackage{array}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{subcaption}
\usetikzlibrary{arrows, patterns, matrix, positioning, calc}


%Information to be included in the title page:
\title{Décomposition de domaine pour l'équation de Helmholtz 1D}
\author{Michaël Kopp}
\institute{Université de Bordeaux}
\date{2021}

\newcommand{\dn}{\ensuremath{\partial_{n}}}
\newcommand{\dOmega}[1][]{\ensuremath{\partial \Omega_{#1}}}
\newcommand{\vcentered}[1]{\begin{tabular}{l} #1 \end{tabular}}

\definecolor{brickred}{rgb}{0.796, 0.255, 0.329}
\definecolor{midnightblue}{rgb}{0.098, 0.098, 0.439}

\graphicspath {{../Resultats/Fig/}}

\begin{document}

\setbeamertemplate{blocks}[rounded]%
    [shadow=true]

\newsavebox{\myasybox}

\frame{\titlepage}

\begin{frame}
    \frametitle{Introduction}        
    Le stage a été proposé par le LJLL et s'est déroulé en intégralité en télétravail en raison de la pandémie du Covid-19.\\
    Les méthodes de Schwarz optimisées ont fait leur apparition dans le domaine des ondes avec la thèse de Bruno Desprès \cite{despr1991},
    qui utilisait des conditions de Robin comme conditions de transmission. Plusieurs développement ont été faits, par exemple la thèse de Matthieu Lecouvez \cite{lecouvez:tel-01444540}
    mais l'opérateur d'échange utilisé jusqu'alors était toujours local. Ce stage a pour but de s'intéresser à une méthode de \textbf{décomposition
    de domaine} pour l'équation de Helmholtz dans le cas \textbf{1D} où l'opérateur d'échange s'incrit dans un contexte plus global.
    Ce qui a été fait s'appuie sur deux articles \cite{refId0} et \cite{claeys2020robust}. L'avantage du 1D est de pouvoir
    détailler le calcul des opérateurs et des constantes qui entrent en jeu analytiquement, et d'observer certains comportements numériquement en des temps raisonnables.\\[2em]
\end{frame}

\begin{frame}
    \textbf{Dans quelles mesures cette nouvelle méthode est-elle novatrice et quelles perspectives
    offre-t-elle quant à la résolution numérique de l'équation de Helmholtz ?}
    \tableofcontents 
\end{frame}

\section{Décomposition de domaine}

\begin{frame}[fragile]
    \frametitle{\thesection. Équation de Helmholtz}
    On veut résoudre $\partial_{tt}^2 u -c^2 \partial_{xx}^2 u = f$, avec $f(x,t)=f(x)e^{-i\omega t}$.\\[1em]
    En cherchant $u(x,t)$ de la forme $u(x)e^{-i\omega t}$ et en se restreignant à $f(x)=0$,
    quitte à renommer $\omega$, on arrive à :\\[1em]
    \begin{minipage}{0.5\linewidth}
        \begin{asy}
            size(150,100);
            pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
            draw(A..B..C..D..E..F..G..cycle);
            label("$\Omega$",(0.8,-0.2));
            label("$\partial \Omega$",(0.9,-0.7));
        \end{asy}
    \end{minipage}\hfill
    \begin{minipage}{0.5\linewidth}
        $\Omega$ ouvert lipchitzien borné.
        \begin{equation*}
            \begin{array}{ll}
                -\Delta u-\omega^2 u=0\text{ sur } \Omega\\
                \dn u -i\omega u =g\text{ sur } \dOmega
            \end{array}
        \end{equation*}
    \end{minipage}\\[1em]
    Problème \emph{a priori} \textbf{non coercif}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Formulation variationnelle}
    En multipliant par $v\in H^1(\Omega)$ quelconque et en utilisant la formule de Green : 
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)=&\int_{\Omega}\partial_x u \partial_x \overline{v} - \omega^2 u \overline{v}\ dx\\
            &-i\omega \int_{\partial \Omega} u\overline{v}\ d\sigma\\[1em]
            l(v)=&\int_{\partial \Omega} g\overline{v}\ d\sigma
        \end{array}
    \end{equation*}    \\[1em]
    Trouver $u\in H^1(\Omega)$ tel que $a(u,v)=l(v)\ \forall v\in H^1(\Omega)$.\\[1em]
    $a$ et $l$ sont sesquilinéaire/anti-linéaire continues dans $H^1(\Omega)$.\\[1em] 
    Problème : $a$ n'est pas \textbf{coercive} dans $H^1(\Omega)$ \dots
\end{frame}

\begin{frame}[fragile]
    \frametitle{\thesection. Décomposition de domaine}
    \textbf{Découper} le domaine en sous-domaines lipchitiziens $\Omega_1, \Omega_2,\dots $
    \newline
    \begin{minipage}{0.4\linewidth}
        \begin{lrbox}{\myasybox}
            \begin{asy}
                size(150,100);
                pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
                draw(A..B..C..D..E..F..G..cycle);
                pair H=(0.3,-0.1), I=(0.6,-0.4), J=(1,-0.3), K=(1.1, 0);
                draw(H..I--J..K--cycle);
        
                draw(A--H);
                draw(B--I);
                draw(C--J);
                draw(D--K);
                label("$\Omega_1$", (0.15, -0.3));
                label("$\Omega_2$", (1, -0.4));
                label("$\Omega_3$", (1.3, -0.1));
                label("$\Omega_4$", (1, 0.1));
                label("$\Omega_5$", (0.7, -0.2));
            \end{asy}
        \end{lrbox}\raisebox{-1.3\height}{\usebox{\myasybox}}
    \end{minipage}\hfill
    \begin{minipage}{0.5\linewidth}
        \begin{equation*}
            \begin{array}{l}
                \overline{\Omega} = \overline{\Omega_1}\cup \dots \cup \overline{\Omega_5}\\
                \Omega_i\cap\Omega_j=\emptyset \text{ quand }i\neq j
            \end{array}
        \end{equation*}
        Décomposition \textbf{sans recouvrement}.
    \end{minipage}\\[1em]
    \begin{minipage}{0.4\linewidth}
        \centering
        \includegraphics[scale=0.2]{fig_ddm.png}
    \end{minipage}\hfill
    \begin{minipage}{0.6\linewidth}
        En pratique, il s'agit de domaines \textbf{polyhédriques}, donc la décomposition est \textbf{simplifiée}.\\[1em]
        En effet, on peut considérer une \textbf{triangulation} de $\Omega$ et choisir les sous-domaines comme unions de simplexes.
    \end{minipage}    
\end{frame}

\begin{frame}[fragile]
    \frametitle{\thesection. Décomposition de domaine}
    \textbf{Découper} le domaine en sous-domaines lipchitiziens $\Omega_1, \Omega_2,\dots $
    \newline
    \begin{tabular}{cl}
        \begin{tabular}{c}
            \begin{lrbox}{\myasybox}
                \begin{asy}
                    size(150,100);
                    pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
                    draw(A..B..C..D..E..F..G..cycle);
                    pair H=(0.3,-0.1), I=(0.6,-0.4), J=(1,-0.3), K=(1.1, 0);
                    draw(H..I--J..K--cycle);

                    pen redpen=red;

                    draw(A--H);
                    draw(B--I, redpen);
                    label("$\Gamma_{12}$", (0.52, -0.5), redpen);
                    draw(C--J);
                    draw(D--K);
                    label("$\Omega_1$", (0.15, -0.3));
                    label("$\Omega_2$", (1, -0.4));
                    label("$\Omega_3$", (1.3, -0.1));
                    label("$\Omega_4$", (1, 0.1));
                    label("$\Omega_5$", (0.7, -0.2));
                \end{asy}
            \end{lrbox}\raisebox{-1.0\height}{\usebox{\myasybox}}
        \end{tabular}
        \begin{tabular}{p{6cm}}
            \begin{itemize}
                \item Raccordement des \textbf{traces Dirichlet} :\\
                $u_1|_{\Gamma_{12}}=u_2|_{\Gamma_{12}}$
            \end{itemize}
        \end{tabular}
    \end{tabular}\\[1em]
    \begin{block}{Propriété de raccordement des fonctions $H^1(\Omega)$}
    $u\in H^1(\Omega) \Longleftrightarrow u_i \in H^1(\Omega_i)\ \forall i \text{ et } \Gamma_{ij}\neq \emptyset \Longrightarrow u_i|_{\Gamma_{ij}}=u_j|_{\Gamma_{ij}}$
    \end{block}
    \emph{Pourquoi faire cela ?}\\
    On peut \textbf{paralléliser} le code en resolvant un problème sur chaque sous-domaine \textbf{indépendamment} des autres, 
    puis utiliser le \textbf{raccordement} via une communication entre les unités de calcul.
\end{frame}

\begin{frame}[fragile]
    \frametitle{\thesection. Nouveaux espaces de solutions}
    Ceci nous amène naturellement à considérer de nouveaux espaces de solutions qui sont \textbf{découplés}.\\[1em]
    $\gamma_D^j$ la trace Dirichlet sur $\partial \Omega_j$ (depuis $\Omega_j$).
    \begin{tabular}{cl}
        \begin{tabular}{c}
            \begin{lrbox}{\myasybox}
                \begin{asy}
                    size(150,100);
                    pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
                    pair H=(0.3,-0.1), I=(0.6,-0.4), J=(1,-0.3), K=(1.1, 0);
            
                    pair AA=A+(0,0.1),FF=F+(0,0.1),EE=E+(0,0.1),DD=D+(0,0.1),HH=H+(0,0.1),KK=K+(0,0.1);
            
                    pair AAA=A-(0.1,0), HHH=H-(0.1,0), III=I-(0.1,0), BBB=B-(0.1,0);
            
                    pair BB=B-(0,0.1), II=I-(0,0.1), JJ=J-(0,0.1), CC=C-(0,0.1);
            
                    pair CCC=C+(0.1,0), JJJ=J+(0.1,0), KKK=K+(0.1,0), DDD=D+(0.1,0);
                    
                    pair AB=0.5*(AAA+BBB)-(0.25,0.25), BC=0.5*(BB+CC)+(0.1,0), CD=0.5*(CCC+DDD)+(0.05,0);
                    
                    
                    pen p = rgb(0.796, 0.255, 0.329);
                    
                    draw(AA..FF..EE..DD--KK--HH--cycle, p);
                    draw(AAA--HHH--III--BBB..AB..cycle, p);
                    draw(BB--II--JJ--CC..BC..cycle, p);
                    draw(CCC..CD..DDD--KKK--JJJ--cycle, p);
            
                    
                    draw(H..I--J..K--cycle, p);
                    label("$\Omega_1$", (0.15, -0.3));
                    label("$\Omega_2$", (0.85, -0.55));
                    label("$\Omega_3$", (1.4, -0.1));
                    label("$\Omega_4$", (1, 0.2));
                    label("$\Omega_5$", (0.7, -0.2));
                    label("$\Sigma$", (1.7, 0.3), p);
                \end{asy}
            \end{lrbox}\raisebox{-1.0\height}{\usebox{\myasybox}}
        \end{tabular}
        \begin{tabular}{p{6cm}}
            \begin{itemize}
                \item Espace volumique :\\
                $\mathbb{V}(\Omega)=\prod_{j=1,J} V(\Omega_j)$
                \item Espace surfacique :\\
                $\mathbb{V}(\Sigma)=\prod_{j=1,J} \gamma_D^j V(\Omega_j)$
            \end{itemize}
        \end{tabular}
    \end{tabular}\\[1cm]
    \textbf{$\Sigma$} est appelé le \textbf{squelette} de la décomposition.
\end{frame}

\section{Application au cas monodimensionnel}

\begin{frame}[fragile]
    \frametitle{\thesection. Espaces multi-trace et simple-trace en 1D}
    L'espace \textbf{multi-trace} peut être vu comme un ensemble de couples sur chaque intervalle.\\[1em]
    \begin{minipage}{0.3\linewidth}
        \begin{asy}
            size(100,100);
            pair A=(0,0), B=(1,0), C=(1.5,0), D=(2.5,0), E=(3,0), F=(4,0);
            dot(A); dot(B); dot(E); dot(F);
            draw(A--C); draw(D--F);
            label("$x_1$", A, S);
            label("$x_2$", B, S);
            label("$x_J$", E, S);
            label("$x_{J+1}$", F, S);
        
            pair AA=(0,1), AB=(0.33,1.2), AC=(0.66,0.8), AD=(1,0.9), 
            BA=(1,0.7), BB=(1.25,0.65), BC=(1.5,0.4), 
            DA=(2.5,0.5), DB=(2.75,0.9), DC=(3,0.8),
            EA=(3,0.6), EB=(3.33,1.1), EC=(3.66,1.2), ED=(4,1.5);
            draw(AA..AB..AC..AD);
            draw(BA..BB..BC);
            draw(DA..DB..DC);
            draw(EA..EB..EC..ED);

            pen redpen = red;

            dot(AA, redpen);
            dot(AD, redpen);
            dot(BA, redpen);

            dot(DC, redpen);
            dot(EA, redpen);
            dot(ED, redpen);
        \end{asy}
    \end{minipage}\hfill
    \begin{minipage}{0.7\linewidth}
        $\mathbb{V}(\Sigma)=\prod_{j=1,J} \gamma_D^j V(I_j)$\\
        $\mathbb{V}(\Sigma)\simeq \mathbb{C}^{2J}$
    \end{minipage}\\[1em]
    L'espace \textbf{simple-trace} est un sous-espace de l'espace multi-trace qui respecte la condition de raccordement aux $x_j, j=2,\dots,J$.\\[1em]
    \begin{minipage}{0.3\linewidth}
        \begin{asy}
            size(100,100);
            pair A=(0,0), B=(1,0), C=(1.5,0), D=(2.5,0), E=(3,0), F=(4,0);
            dot(A); dot(B); dot(E); dot(F);
            draw(A--C); draw(D--F);
            label("$x_1$", A, S);
            label("$x_2$", B, S);
            label("$x_J$", E, S);
            label("$x_{J+1}$", F, S);
            
            pair AA=(0,1), AB=(0.33,1.2), AC=(0.66,0.8), AD=(1,0.9), 
            BA=(1,0.7), BB=(1.25,0.65), BC=(1.5,0.4), 
            DA=(2.5,0.5), DB=(2.75,0.4), DC=(3,0.8),
            EA=(3,0.6), EB=(3.33,1), EC=(3.66,1.2), ED=(4,1.5);
            draw(AA..AB..AC..BA);
            draw(BA..BB..BC);
            draw(DA..DB..EA);
            draw(EA..EB..EC..ED);

            pen redpen = red;

            dot(AA, redpen);
            dot(BA, redpen);
            dot(EA, redpen);
            dot(ED, redpen);
        \end{asy}
    \end{minipage}\hfill
    \begin{minipage}{0.7\linewidth}
        $V(\Sigma)\simeq\left\{z\in \mathbb{C}^{2J}\mid z_{2j}=z_{2j+1}, j=1,\dots,J-1\right\}$\\
        $V(\Sigma)\simeq \mathbb{C}^{J+1}$
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Formulation variationnelle étendue}
    Pour $(u,v)\in \mathbb{V}(I)$,
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)=&\textcolor{brickred}{\sum_{j=1,J}\int_{x_j}^{x_{j+1}}\partial_x u_j \partial_x \overline{v_j} - \omega^2 u_j \overline{v_j}\ dx}\\
            &\textcolor{midnightblue}{-i\omega\left[u_1(x_1)\overline{v_1}(x_1)+u_J(x_{J+1})\overline{v_J}(x_{J+1})\right]}\\[1cm]
            l(v)=&\textcolor{brickred}{0}+\textcolor{midnightblue}{g(x_{J+1})\overline{v_J}(x_{J+1})+g(x_1)\overline{v_1}(x_1)}
        \end{array}
    \end{equation*}    
    \\[1cm] 
    Trouver $u\in \textcolor{brickred}{\mathbb{V}(I)}$ tel que 
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)=l(v)\ \forall v\in \textcolor{brickred}{V(I)}\\
            u|_\Sigma \in  \textcolor{midnightblue}{V(\Sigma)}
        \end{array}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Reformulation}
    Introduction d'un \textbf{produit scalaire} sur $\mathbb{V}(\Sigma)$ : $Z(.,.)$\\[1em]
    En 1D, il s'agit d'une \textbf{matrice symétrique définie positive} car $\mathbb{V}(\Sigma)$ est de \textbf{dimension finie}. 
    Elle induit une norme sur $\mathbb{V}(\Sigma)$ : $\left\|\ \right\|_Z$\\[2em]

    Trouver $(u,q)\in \textcolor{brickred}{\mathbb{V}(I)}\times \textcolor{midnightblue}{V(\Sigma)^\perp}$ tel que 
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)-Z(q,v|_\Sigma)=l(v)\ \forall v\in \textcolor{brickred}{\mathbb{V}(I)}\\
            Z(u|_\Sigma,p)=0\ \forall p\in \textcolor{midnightblue}{V(\Sigma)^\perp}
        \end{array}
    \end{equation*}
    Cette fois, $v\in \textcolor{brickred}{\mathbb{V}(I)}$ et $a(u,v)-Z(q,v|_\Sigma)$ est \textbf{diagonale par block} $\Longrightarrow$ \textbf{parallélisable}.
\end{frame}

\begin{frame}[fragile]
    \frametitle{\thesection. Opérateur d'échange}
    But : Reformuler pour chercher $q$ dans un espace \textbf{découplé}.\\
    $\Pi$ opérateur de symétrie Z-orthogonal par rapport à $V(\Sigma)$.\\[1cm]
    En notant $P$ la projection $Z$-orthogonale sur $\textcolor{midnightblue}{V(\Sigma)}$ : $\Pi=2P-I$.
    \begin{block}{Propriété de découplage}
        Soit $(u|_\Sigma,q)\in\textcolor{midnightblue}{\mathbb{V}(\Sigma)}\times\textcolor{midnightblue}{\mathbb{V}(\Sigma)}$,\\
        $(u|_\Sigma,q)\in \textcolor{midnightblue}{V(\Sigma)} \times \textcolor{midnightblue}{V(\Sigma)^\perp} \Longleftrightarrow \Pi(q+iu|_\Sigma)=iu|_\Sigma-q$
    \end{block} 
    \begin{figure}
        \centering
        \begin{asy}
            size(150,150);
            pair A=(0,0), B=(1,1), C=(0,1), D=(1,0);
            draw(A--B);
            draw(C--D);
    
            pair qp=(0.9,0.6);
            pair qm=(0.4,0.1);
            dot(qp);
            dot(qm);
            label("$q+iu|_\Sigma$", qp, NE);
            label("$iu|_\Sigma-q$", qm, S);
            label("$V(\Sigma)$", C, SW);
            label("$V(\Sigma)^\perp$", A, SW);
        \end{asy}
    \end{figure}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\thesection. Opérateur d'échange}
    Nous obtenons alors une nouvelle formulation du problème, \textbf{découplée} cette fois :\\[1em]
    Trouver $(u,q)\in \textcolor{brickred}{\mathbb{V}(I)}\times \textcolor{midnightblue}{\mathbb{V}(\Sigma)}$ tel que 
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)-Z(q,v|_\Sigma)=l(v)\ \forall v\in \textcolor{brickred}{\mathbb{V}(I)}\\
            q-iu|_\Sigma=-\Pi(q+iu|_\Sigma)
        \end{array}
    \end{equation*}
    \begin{figure}
        \centering
        \begin{asy}
            size(150,150);
            pair A=(0,0), B=(1,1), C=(0,1), D=(1,0);
            draw(A--B);
            draw(C--D);
    
            pair qp=(0.9,0.6);
            pair qm=(0.4,0.1);
            dot(qp);
            dot(qm);
            label("$q+iu|_\Sigma$", qp, NE);
            label("$iu|_\Sigma-q$", qm, S);
            label("$V(\Sigma)$", C, SW);
            label("$V(\Sigma)^\perp$", A, SW);
        \end{asy}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Opérateur de scattering}
    Changement de variable $p=q-iu|_\Sigma$ pour assurer le caractère bien posé.

    Trouver $(u,p)\in \textcolor{brickred}{\mathbb{V}(I)}\times \textcolor{midnightblue}{\mathbb{V}(\Sigma)}$ tel que 
    \begin{equation*}
        \begin{array}{ll}
            a(u,v)-iZ(u|_\Sigma,v)=Z(p,v|_\Sigma)+l(v)\ \forall v\in \textcolor{brickred}{\mathbb{V}(I)}\\
            p=-\Pi(p+2iu|_\Sigma)
        \end{array}
    \end{equation*}
    On écrit $u=w+u_{*}$ avec $w$ solution de l'équation \textbf{sans terme source} et on définit en 
    suivant $Sp=p+2iw|_\Sigma$ et $f=-2i\Pi u_{*}$.\\[1em]
    \textbf{Nous avons à résoudre $(I+\Pi S)p=f$ avec $p\in\textcolor{midnightblue}{\mathbb{V}(\Sigma)}$.}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Opérateur de scattering}
    En choisissant $\omega$ dans le quart de plan complexe on obtient le résultat suivant :\\[1em]
    \begin{block}{$S$ est contractant}
        $\forall p\in \mathbb{V}(\Sigma), \left\|Sp\right\|_Z\leq \left\|p\right\|_Z$ 
    \end{block}
    De quoi découle la propriété de
    \begin{block}{Coercivité pour $I+\Pi S$}
        $\forall p \in \mathbb{V}(\Sigma),\Re \{Z(p,(I+\Pi S)p)\}\geq \frac{\textcolor{brickred}{\gamma}^2}{2}\left\|p\right\|_Z^2$\\[1em]
        avec $\textcolor{brickred}{\gamma}=\underset{p\in \mathbb{V}(\Sigma)}{\inf}\frac{\left\|(I+\Pi S)p\right\|_Z}{\left\|p\right\|_Z}>0$.
    \end{block}
    $\gamma$ n'est \emph{a priori} pas connu, on peut toutefois le calculer numériquement à l'aide d'une SVD.
\end{frame}

\section{Résolution numérique}

\begin{frame}
    \frametitle{\thesection. Algorithme itératif}
    Algorithme de Richardson avec un paramètre $r$, choisi heuristiquement. On prendra $r=\frac{1}{\sqrt{2}}$.
    $$
    \begin{array}{ll}
        (i) & p^{(n)}=(1-r)p^{(n-1)}-r\Pi S(p^{(n-1)}+2iu^{(n-1)}|_{\Sigma})\\
        (ii) & a(u^{(n)},v)-iZ(u^{(n)}|_\Sigma,v|_\Sigma)=Z(p^{(n)},v|_\Sigma)+l(v)
    \end{array}
    $$
    Nous disposons d'une \textbf{borne} pour la convergence relative en norme : 
    \begin{block}{Borne pour la convergence de l'algorithme}
        $\forall n\in \mathbb{N}, \frac{||p^{(n)}-p^{(\infty)}||_Z}{||p^{(0)}-p^{(\infty)}||_Z} \leq (1-r(1-r)\gamma^2)^\frac{n}{2}$
    \end{block}
    Plus $\gamma$ est proche de $0$, moins la borne est bonne.
\end{frame}

\begin{frame}
    \frametitle{\thesection. Résultats numériques}
    On fixe $\omega=10$, $I=\left]0,1\right[$ et $g$ tel que $u(x)=e^{i\omega x}$.\\[1em]
    On s'intéresse à deux produits scalaires $Z=\omega I$ et $Z=DtN$.\\[1em]
    $DtN$ est l'opérateur Dirichlet to Neumann associé au problème $-\Delta u + \omega^2 u=0$.
    C'est l'opérateur associé à un problème de minimisation d'une certaine norme dans $H^1(I)$.
\end{frame}

\begin{frame}
    \frametitle{\thesection. Spectre de $A=I+\Pi S$}
    \begin{figure}[h]
        \begin{subfigure}[b]{0.4\textwidth}
            \centering
        \includegraphics[width=\textwidth]{spectre_A_Id_DtN_100_ss_domaines.png}
        \end{subfigure}
        \begin{subfigure}[b]{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{spectre_A_Id_DtN_100_ss_domaines_zoom.png}     
        \end{subfigure}        
        \caption{Spectre de $A=I+\Pi S$ pour $J=100$}        
    \end{figure}
    $A$ est une \textbf{matrice} et on a bien $\left|\lambda\right|\leq 1$.\\[1em]
    De manière générale $\gamma\leq \left|\lambda_{min}\right|$ (avec égalité dans le cas où $A$ est symétrique semi-définie positive).
\end{frame}

\begin{frame}
    \frametitle{\thesection. SVD de $A=I+\Pi S$ pour le produit scalaire $Z$}
    \begin{figure}
        \includegraphics[scale=0.5]{min_svd_100_ss_domaines.png}
        \caption{SVD de $A=I+\Pi S$ dans le produit scalaire $Z$ pour $J=100$}       
    \end{figure}
    La plus petite valeur singulière de $A$ est plus éloignée de l'origine pour 
    $Z=DtN$ que pour $Z=\omega I$. On peut donc s'attendre à un \textbf{meilleur taux de convergence} 
    dans le premier cas \dots
\end{frame}

\begin{frame}
    \frametitle{\thesection. Convergence relative en norme $Z$ avec les bornes}
    \begin{figure}
        \includegraphics[scale=0.4]{borne_100_ss_domaines_Z=Id_et_Z=DtN.png}

        On a effectivement une meilleure convergence pour $Z=DtN$\\[1em]
        On peut s'intéresser à l'évolution de $\gamma$ en fonction du nombre de sous-domaines. 
        On peut s'attendre à une détérioration.
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Détérioration des bornes théoriques}
    \begin{figure}
        \includegraphics[scale=0.4]{DeteriorationGamma_Z=Id_et_Z=DtN.png}\\[1em]
        Détériotation asymptotique \textbf{inversement linéaire} de $\gamma$ en fonction du nombre de sous-domaines.
        Le choix de $Z=DtN$ semble plus consistant pour un nombre de domaines suffisamment élevé.
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\thesection. Déterioration de la vitesse de convergence en pratique}
    \begin{figure}
        \includegraphics[scale=0.4]{erreur_relative_Z=Id.png}\\[1em]
        La détérioration pratique du  taux de convergence en fonction du nombre de sous-domaines est bien 
        \textbf{cohérente} avec la théorie.
    \end{figure}
\end{frame}

\begin{frame}
    \centering
    {\LARGE Merci pour votre attention !}    
\end{frame}

\begin{frame}
    {\footnotesize \bibliography{biblio}}
    \bibliographystyle{plain}
\end{frame}
\end{document}