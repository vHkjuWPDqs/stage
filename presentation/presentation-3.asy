if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="presentation-3";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

size(150,100);
pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
draw(A..B..C..D..E..F..G..cycle);
pair H=(0.3,-0.1), I=(0.6,-0.4), J=(1,-0.3), K=(1.1, 0);
draw(H..I--J..K--cycle);

pen redpen=red;

draw(A--H);
draw(B--I, redpen);
label("$\Gamma_{12}$", (0.52, -0.5), redpen);
draw(C--J);
draw(D--K);
label("$\Omega_1$", (0.15, -0.3));
label("$\Omega_2$", (1, -0.4));
label("$\Omega_3$", (1.3, -0.1));
label("$\Omega_4$", (1, 0.1));
label("$\Omega_5$", (0.7, -0.2));
