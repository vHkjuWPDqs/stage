if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="presentation-4";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

size(150,100);
pair A=(0,0), B=(0.65,-0.6), C=(1.5,-0.2), D=(1.6, 0.1), E=(1.2, 0.3), F=(0.7, 0.2), G=(0.3, 0.1);
pair H=(0.3,-0.1), I=(0.6,-0.4), J=(1,-0.3), K=(1.1, 0);

pair AA=A+(0,0.1),FF=F+(0,0.1),EE=E+(0,0.1),DD=D+(0,0.1),HH=H+(0,0.1),KK=K+(0,0.1);

pair AAA=A-(0.1,0), HHH=H-(0.1,0), III=I-(0.1,0), BBB=B-(0.1,0);

pair BB=B-(0,0.1), II=I-(0,0.1), JJ=J-(0,0.1), CC=C-(0,0.1);

pair CCC=C+(0.1,0), JJJ=J+(0.1,0), KKK=K+(0.1,0), DDD=D+(0.1,0);

pair AB=0.5*(AAA+BBB)-(0.25,0.25), BC=0.5*(BB+CC)+(0.1,0), CD=0.5*(CCC+DDD)+(0.05,0);


pen p = rgb(0.796, 0.255, 0.329);

draw(AA..FF..EE..DD--KK--HH--cycle, p);
draw(AAA--HHH--III--BBB..AB..cycle, p);
draw(BB--II--JJ--CC..BC..cycle, p);
draw(CCC..CD..DDD--KKK--JJJ--cycle, p);


draw(H..I--J..K--cycle, p);
label("$\Omega_1$", (0.15, -0.3));
label("$\Omega_2$", (0.85, -0.55));
label("$\Omega_3$", (1.4, -0.1));
label("$\Omega_4$", (1, 0.2));
label("$\Omega_5$", (0.7, -0.2));
label("$\Sigma$", (1.7, 0.3), p);
