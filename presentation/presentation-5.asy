if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="presentation-5";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

size(100,100);
pair A=(0,0), B=(1,0), C=(1.5,0), D=(2.5,0), E=(3,0), F=(4,0);
dot(A); dot(B); dot(E); dot(F);
draw(A--C); draw(D--F);
label("$x_1$", A, S);
label("$x_2$", B, S);
label("$x_J$", E, S);
label("$x_{J+1}$", F, S);

pair AA=(0,1), AB=(0.33,1.2), AC=(0.66,0.8), AD=(1,0.9),
BA=(1,0.7), BB=(1.25,0.65), BC=(1.5,0.4),
DA=(2.5,0.5), DB=(2.75,0.9), DC=(3,0.8),
EA=(3,0.6), EB=(3.33,1.1), EC=(3.66,1.2), ED=(4,1.5);
draw(AA..AB..AC..AD);
draw(BA..BB..BC);
draw(DA..DB..DC);
draw(EA..EB..EC..ED);

pen redpen = red;

dot(AA, redpen);
dot(AD, redpen);
dot(BA, redpen);

dot(DC, redpen);
dot(EA, redpen);
dot(ED, redpen);
