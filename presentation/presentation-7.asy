if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="presentation-7";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

size(150,150);
pair A=(0,0), B=(1,1), C=(0,1), D=(1,0);
draw(A--B);
draw(C--D);

pair qp=(0.9,0.6);
pair qm=(0.4,0.1);
dot(qp);
dot(qm);
label("$q+iu|_\Sigma$", qp, NE);
label("$iu|_\Sigma-q$", qm, S);
label("$V(\Sigma)$", C, SW);
label("$V(\Sigma)^\perp$", A, SW);
