\documentclass{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{tikz}
\usetikzlibrary{matrix,positioning}
\usepackage{kbordermatrix}
\usepackage{blkarray}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{bbm}

\newtheorem*{remark}{Remark}

\newcommand{\dx}{\ensuremath{\partial_{x}}}
\newcommand{\dn}{\ensuremath{\partial_{n}}}
\newcommand{\dxx}{\ensuremath{\partial_{xx}^{2}}}
\newcommand{\dOmega}{\ensuremath{\partial \Omega}}
\newcommand{\dGamma}{\ensuremath{\partial \Gamma}}

\newcommand{\dOmegaimdemi}{\ensuremath{\partial\Omega_{k-\frac{1}{2}}}}
\newcommand{\Omegaimdemi}{\ensuremath{\Omega_{k-\frac{1}{2}}}}
\newcommand{\uimdemi}{\ensuremath{u_{k-\frac{1}{2}}}}

\newcommand{\dOmegaidemi}{\ensuremath{\partial\Omega_{k+\frac{1}{2}}}}
\newcommand{\Omegaidemi}{\ensuremath{\Omega_{k+\frac{1}{2}}}}
\newcommand{\uidemi}{\ensuremath{u_{k+\frac{1}{2}}}}

\newcommand{\dOmegaitdemi}{\ensuremath{\partial\Omega_{k+\frac{3}{2}}}}
\newcommand{\Omegaitdemi}{\ensuremath{\Omega_{k+\frac{3}{2}}}}
\newcommand{\uitdemi}{\ensuremath{u_{k+\frac{3}{2}}}}

\newcommand{\fidemi}{\ensuremath{f_{k+\frac{1}{2}}}}
\newcommand{\Gammai}[1]{\ensuremath{\Gamma_{#1}}}

\newcommand{\uirest}[2]{\ensuremath{u_{{k+\frac{#1}{2}}_{|_{\Gamma_{#2}}}}}}
\newcommand{\uimrest}[2]{\ensuremath{u_{{k-\frac{#1}{2}}_{|_{\Gamma_{#2}}}}}}

\newcommand{\OmegaMeasure}{\ d\Omega}
\newcommand{\GammaMeasure}{\ d\Gamma}
\newcommand{\vb}{\ensuremath{\overline{v}}}

\newcommand{\Ldzu}{\ensuremath{{L^{2}(\left]0,1\right[)}}}

\begin{document}
    \section{Problème de Helmholtz 1D}
    \subsection{Problème global}
Soit $\Omega$ un ouvert non vide borné lipchitzien et connexe de $\mathbb{R}$ : $\Omega=\left]a,b\right[$, $a>b$.
Le problème de Helmholtz revient à résoudre le système

\begin{equation}\label{eq1}
    \left\{
        \begin{array}[]{ll}
            -\dxx u - \omega^{2} u &= f \text{ dans $\Omega$} \\
            \dn u + i\omega u &= 0 \text{ sur $\dOmega$}
        \end{array}
    \right.  
\end{equation}
\subsection{Décomposition de domaine}
Dans la présente section, nous nous intéressons aux méthodes de décomposition de domaine.
Pour l'illustrer sur notre exemple, il nous faut par exemple choisir une partition du domaine $\Omega$ : $(x_{k})_{k\in\left[0,K\right]}$
que l'on notera comme suit
$$
a=x_{0}<...<x_{K}=b
$$
Cette méthode consiste donc à diviser le problème global (qui a lieu sur tout le domaine) en problèmes de 
taille plus modeste sur des sous-domaines, reliés par certaines conditions de "transmission". 
Il s'agit en réalité de raccorder les solutions particulières à la solution générale. Étant donné
que dorénavant nous étudirons cette approche, il nous faut convenir de notations particulières à chaque domaine.
Nous adopterons donc les notations suivantes : 
$$
\begin{array}[]{llrl}
    \Omegaidemi&=\left]x_{k}, x_{k+1}\right[, &&k=0,...,K-1 \\
    \uidemi&=u_{|_{\Omegaidemi}}, &&k=0,...,K-1 \\
    \fidemi&=f_{|_{\Omegaidemi}}, &&k=0,...,K-1\\
    \Gammai{k}&=\dOmegaimdemi\cap\dOmegaidemi, &&k=0,...,K \\
\end{array}
$$
En convenant que $\partial\Omega_{-\frac{1}{2}}=\partial\Omega_{K+\frac{1}{2}}=\dOmega$. Nous conviendrons également que 
$u_{-\frac{1}{2}}=u_{K+\frac{1}{2}}=0$.
\paragraph{}
Par continuité des traces Dirichlet et Neumann et le caractère lipchitzien des sous-domaines, le problème (\ref{eq1}) est équivalent au problème
$$
\left\{
    \begin{array}[]{rlrl}
       -\dxx \uidemi - \omega^{2} \uidemi &= \fidemi \text{ dans $\Omegaidemi$} && k=0,...,K-1 \\
        \dn \uidemi + i\omega \uidemi &= 0 \text{ sur $\dOmega\cap\Omegaidemi$} && k=0,...,K-1 \\ \\
        \uirest{1}{k+1} &= \uirest{3}{k+1} && k=0,...,K-2 \\
        \dn \uirest{1}{k+1} &=-\dn \uirest{3}{k+1} && k=0,...,K-2
    \end{array}
\right.
$$
qui se réécrit par une combinaison linéaire sur les conditions de transmission
$$
\left\{
    \begin{array}[]{llrl}
        -\dxx \uidemi - \omega^{2} \uidemi &= \fidemi \text{ dans $\Omegaidemi$} && k=0,...,K-1 \\ \\
        i\omega\ \uirest{1}{k+1} + \dn \uirest{1}{k+1} &= i\omega\ \uirest{3}{k+1} - \dn \uirest{3}{k+1} && k=0,...,K-1\\
        -i\omega\ \uirest{1}{k+1} + \dn \uirest{1}{k+1} &= -i\omega\ \uirest{3}{k+1} - \dn \uirest{3}{k+1} && k=-1,...,K-2
    \end{array}
\right.
$$
Si nous nous plaçons sur un sous-domaine $\Omegaidemi$ fixé, nous pouvons noter que la connaissance
de la solution sur les sous-domaines voisins est nécessaire, du moins leur trace Dirichlet et Neumann.
Pour pallier ce problème nous allons utiliser un algorithme itératif qui suit le schéma suivant
$$
\left\{
    \begin{array}[]{llrl}
        -\dxx \uidemi^{(n+1)} - \omega^{2} \uidemi^{(n+1)} &= \fidemi \text{ dans $\Omegaidemi$} && k=0,...,K-1 \\ \\
        i\omega\ \uirest{1}{k+1}^{(n+1)} + \dn \uirest{1}{k+1}^{(n+1)} &= i\omega\ \uirest{3}{k+1}^{(n)} - \dn \uirest{3}{k+1}^{(n)} && k=0,...,K-1\\
        i\omega\ \uirest{1}{k}^{(n+1)} + \dn \uirest{1}{k}^{(n+1)} &= i\omega\ \uimrest{1}{k}^{(n)} - \dn \uimrest{1}{k}^{(n)} && k=0,...,K-1
    \end{array}
\right.
$$
Ainsi, à l'itération $n+1$, la solution de l'itération $n$ est connue sur les sous-domaines voisins et peut donc être considérée
comme une donnée du problème $h^{(n)}$ et $g^{(n)}$. Il a été montré dans \cite{DDHW} que l'algorithme converge en $K$ itérations. 
Sur un sous-domaine fixé $\Omegaidemi$ à l'itération $n+1$, nous avons donc la formulation variationnelle équivalente

\begin{equation}\label{eq2}
    \left\{
        \begin{array}[]{ll}
            \text{Trouver $u\in H^{1}(\Omegaidemi)$}\\
            a(u,v)=l(v) \ \forall v \in H^{1}(\Omegaidemi)
        \end{array}
    \right.
\end{equation}
avec 
$$    a(u,v)=\int_{\Omegaidemi} \dx u\ \dx \vb \OmegaMeasure + i\omega\int_{\dOmegaidemi} u\vb \GammaMeasure - \omega^{2}\int_{\Omegaidemi} u\vb \OmegaMeasure$$
$$    l(v)=\int_{\Gammai{k}} g^{(n)}\vb \GammaMeasure + \int_{\Gammai{k+1}} h^{(n)}\vb \GammaMeasure + \int_{\Omegaidemi} f\vb \OmegaMeasure$$
%A changer suite à la modif de l'équation
% \subsection{Lax-Milgram}
% La forme sesquilinéaire $a$ est clairement continue par le théorème de trace et l'inégalité de Cauchy-Schwartz. 
% Il en va de même pour la forme antilinéaire $l$. L'espace $H^{1}(\Omegaidemi)$ est un Hilbert
% et puisque $\omega\in\mathbb{R}$, $a$ est clairement coercive de constante de coercivité $\min(1,\omega^{2})$.
% On peut ainsi appliquer le théorème de Lax-Milgram et on obtient un unique $u\in H^{1}(\Omegaidemi)$ solution du problème,
% de plus on a l'estimation $||u||_{H^{1}(\Omegaidemi)}\leq \frac{1}{\min(1, \omega^{2})}||l||_{\mathcal{L}(H^{1}(\Omegaidemi),\mathbb{C})}$
\subsection{Cas particulier $f=0$}
\subsubsection{Raccord des traces Robin}
Dans ce paragraphe nous nous proposons d'étudier le lien entre les traces Robin sortantes et entrantes sur un même sous-domaine
fixé que l'on appellera $I=\left]c,d\right[$. Nous avons donc le problème suivant
\begin{equation}
    \left\{
        \begin{array}[]{ll}
            -\dxx u - \omega^{2} u &= 0 \text{ dans $I$} \\
            -\dx u + i\omega u &= \alpha \text{ en c }\\
            \dx u + i\omega u &= \beta { en d }
        \end{array}
    \right.  
\end{equation}
Tout d'abord, la solution générale est de la forme $u(x)=Ae^{-iwc}+Be^{iwx}$. Maintenant, en convenant que $n=-1$ en $c$ et $n=+1$ en $d$
$$\dn u+i\omega u=\alpha,\beta \text{ sur le bord } \Leftrightarrow i\omega ((A-An)e^{-iwx}+(B+Bn)e^{iwx})=\alpha,\beta$$
Ce qui, en prenant les valeurs au bord, donne
$$
\begin{array}{ll}
    A=\frac{\alpha}{2i\omega}e^{i\omega c}\\
    B=\frac{\beta}{2i\omega}e^{-i\omega d}
\end{array}
$$
D'où la trace de Robin entrante
$$
\begin{array}{ll}
    -\dn u + iwu    &=i\omega ((A+An)e^{-iwx}+(B-Bn)e^{iwx})\\
                    &=\left\{
                        \begin{array}{ll}
                            \beta e^{-iwh} \text{ en c}\\
                            \alpha e^{-iwh} \text{ en d}
                        \end{array}
                    \right.
\end{array}$$
avec $h=d-c$.\\
En notant $T_{e}$ et $T_{s}$ les opérateurs de trace entrante et sortante, nous obtenons ainsi la relation suivante
\begin{equation}\label{eq5}
\begin{pmatrix}
    T_{e}u(c)\\
    T_{e}u(d)
\end{pmatrix}
=
\begin{pmatrix}
    0 & e^{-iwh}\\
    e^{-iwh} & 0
\end{pmatrix}
\begin{pmatrix}
    T_{s}u(c)\\
    T_{s}u(d)
\end{pmatrix}
\end{equation}
Nous sommes donc capables sur chaque sous-domaine de calculer les traces entrantes en fonction des traces sortantes.
Si nous reprenons notre subdivision $x_{0},...,x_{K}$, alors les conditions de transmission (et les conditions de bord) s'écrivent
\begin{equation}\label{eq6}
    \begin{pmatrix}
        T_{s,\frac{1}{2}}(x_{0})\\
        T_{s,\frac{1}{2}}(x_{1})\\
        T_{s,\frac{3}{2}}(x_{1})\\
        T_{s,\frac{3}{2}}(x_{2})\\
        \hdots         \\
        T_{s,K-\frac{3}{2}}(x_{K-2})\\
        T_{s,K-\frac{3}{2}}(x_{K-1})\\
        T_{s,K-\frac{1}{2}}(x_{K-1})\\
        T_{s,K-\frac{1}{2}}(x_{K})\\
    \end{pmatrix}
    =\begin{pmatrix}
        1       & 0         & 0         & \hdots    & \hdots    & \hdots    & \hdots    & \hdots    & 0\\
        0       & 0         & 1         & \ddots    &           &           &           &           & \vdots\\
        0       & 1         & 0         & 0         & \ddots    &           &           &           & \vdots\\
        \vdots  & \ddots    & 0         & 0         & 1         & \ddots    &           &           & \vdots\\
        \vdots  &           & \ddots    & 1         & 0         & \ddots    & \ddots    &           & \vdots\\
        \vdots  &           &           & \ddots    & \ddots    &\ddots     & \ddots    & \ddots    & 0\\
        \vdots  &           &           &           & \ddots    & \ddots    & 0         & 1         & 0\\
        \vdots  &           &           &           &           & \ddots    & 1         & 0         & 0\\
        0       & \hdots    & \hdots    & \hdots    &\hdots     & \hdots    & 0         & 0         & 1
    \end{pmatrix}
    \begin{pmatrix}
        g(x_{0})\\
        T_{e,\frac{1}{2}}(x_{1})\\
        T_{e,\frac{3}{2}}(x_{1})\\
        T_{e,\frac{3}{2}}(x_{2})\\
        \hdots         \\
        T_{e,K-\frac{3}{2}}(x_{K-2})\\
        T_{e,K-\frac{3}{2}}(x_{K-1})\\
        T_{e,K-\frac{1}{2}}(x_{K-1})\\
        g(x_{K})\\
    \end{pmatrix}
\end{equation}
En notant $v_{k+1}$ les traces sortantes à l'itération $k+1$, on obtient alors la relation de récurrence
$v_{k+1}=\Pi S v_{k}$ où $\Pi$ est la plus matrice de (\refeq{eq6}) et $S$ est formée à l'aide de (\refeq{eq5}). $\Pi$ est appelé {\it opérateur d'échange}
et $S$ {\it opérateur de scattering}.
\subsubsection{Opérateur Dirichlet to Neumann}
De manière similaire à ce qui vient d'être fait précédement, nous nous proposons dans ce paragraphe de calculer l'opérateur {\it Dirichlet to Neumann}
dans le cas où notre second membre est nul, sur un intervalle $I=\left]c,d\right[$.
$$
\left\{
        \begin{array}{l}
            -\dxx u - \omega^{2} u = 0 \text{ dans $I$} \\
            u(c) = \alpha\\
            u(d) = \beta
        \end{array}
\right.  
$$
D'après la forme générale de la solution $u(x)=Ae^{-i\omega x}+Be^{i\omega x}$, les conditions de Dirichlet au bord imposent
$$
\begin{pmatrix}
    e^{-i\omega c} & e^{i\omega c}\\
    e^{-i\omega d} & e^{i\omega d}
\end{pmatrix}
\begin{pmatrix}
    A\\
    B
\end{pmatrix}
=\begin{pmatrix}
    \alpha\\
    \beta
\end{pmatrix}
$$
Or $$\dn u(x)=i\omega n
\begin{pmatrix}
    -e^{-i\omega x} & e^{i\omega x}
\end{pmatrix}
\begin{pmatrix}
    A\\
    B
\end{pmatrix}
$$
D'où l'on tire finalement (lorsque $\omega h \neq 0 \mod{\pi}$, avec $h=d-c$)
$$
\begin{array}{ll}
    \begin{pmatrix}
        \dn u(c)\\
        \dn u(d)
    \end{pmatrix}
\end{array}
=i\omega \begin{pmatrix}
    e^{-i\omega c} & -e^{i\omega c}\\
    -e^{-i\omega d} & e^{i\omega d}
\end{pmatrix}
{\begin{pmatrix}
    e^{-i\omega c} & e^{i\omega c}\\
    e^{-i\omega d} & e^{i\omega d}
\end{pmatrix}}^{-1}
\begin{pmatrix}
    u(c)\\
    u(d)
\end{pmatrix}
$$
Cette matrice qui permet de passer des conditions de Dirichlet aux conditions de Neumann est l'opérateur {\it Dirichlet to Neumann}
dans notre cas bien précis. Nous l'appelerons dorénavant $T$ et nous avons l'expression suivante
$$
T=\omega \begin{pmatrix}
    \text{cotan}(\omega h) & -\text{cosec}(\omega h)\\
    -\text{cosec}(\omega h) & \text{cotan}(\omega h)
\end{pmatrix}
$$
dont la diagonalisation donne
$$
T=\begin{pmatrix}
    \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}\\
    -\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}
\end{pmatrix}
\begin{pmatrix}
    \text{cotan}(\omega h) + \text{cosec} (\omega h) & 0\\
    0 & \text{cotan}(\omega h) - \text{cosec} (\omega h)
\end{pmatrix}
\begin{pmatrix}
    \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}\\
    \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}
\end{pmatrix}
$$
% \begin{remark}[Valeurs propres]\mbox{}
%     \begin{itemize}
%         \item Quand $\omega h\rightarrow 0^{+} \mod{\pi}$, $\lambda_{1}\rightarrow+\infty$
%         \item Quand $\omega h\rightarrow 0^{-} \mod{\pi}$, $\lambda_{1}\rightarrow-\infty$
%         \item Quand $\omega h\rightarrow \pi^{+} \mod{2\pi}$, $\lambda_{2}\rightarrow+\infty$
%         \item Quand $\omega h\rightarrow \pi^{-} \mod{2\pi}$, $\lambda_{2}\rightarrow-\infty$
%         \item Quand $|\omega h|\rightarrow 0 \mod{2\pi}$, $\lambda_{2}\rightarrow 0$
%     \end{itemize}    
% \end{remark}
\subsubsection{Expression de l'opérateur de scattering local en fonction de l'opérateur Dirichlet to Neumann}
Commençons tout d'abord par généraliser les conditions de transmission. Il est tout à fait équivalent de considérer
les conditions avec les expressions $\pm \dn u_{k} + i\omega Z u_{k}$ à la place de $\pm\dn u_{k} + i\omega u_{k}$ du moment
que l'on choisit un opérateur $Z$ qui convienne, nous y reviendrons par la suite. Nous avons alors les expressions
des traces entrantes et sortantes en fonction de $T$ et $Z$
$$
\begin{array}{l}
    T_{e}=-T+i\omega Z\\
    T_{s}=T+i\omega Z
\end{array}
$$
Pour peu que $Z$ ne soit pas anti-adjoint, nous avons $$T_{e}=(-T+i\omega Z)(T+i\omega Z)^{-1}T_{s}$$ qui nous permet d'avoir
une expression pour l'opérateur de scattering local.

\subsection{Méthode des éléments finis}
\subsubsection{Méthode de Galerkin}
Le problème étant posé dans un espace de dimension infinie, nous allons l'approcher par des espaces de dimension finie $V_{h}$ avec $\dim(V_{h})=n$ et $V_{h} \subset H^{1}(\Omegaidemi)$ de sorte que la solution 
approchée se rapproche de la véritable solution lorsque $h\rightarrow 0$. Si l'on note $(\varphi^{h}_{i})_{i\in\left[1,n\right]}$
une base de cet espace $V_{h}$, alors nous obtenons de (\ref{eq2}) l'équation suivante :
\begin{equation}\label{eq3}
    \forall i \in \left[1,n\right], a(u, \varphi^{h}_{i})=l(\varphi^{h}_{i})
\end{equation}
Nous nous contenterons alors de trouver une solution approchée dans cet espace de dimension finie : $\widetilde{u}=\sum_{j=1,n}c_{j}\varphi^{h}_{j}$.
En notant $C$ le vecteur formé par les $(c_{j})_{j\in\left[1,n\right]}$,
$A$ la matrice formée par les $(a(\varphi^{h}_{j}, \varphi^{h}_{i}))_{(i,j)\in\left[1,n\right]^{2}}$ et enfin $b$ le vecteur formé par
les $(l(\varphi^{h}_{i}))_{i\in\left[1,n\right]}$, alors (\ref{eq3}) "approché" devient
\begin{equation}\label{eq4}
    Ac=b
\end{equation}
\subsubsection{Éléments finis de Lagrange $\mathbb{P}_2$}
Supposons que le problème se situe sur un intervalle $I$ subdivisé en intervalles $(I_k)_{k\in\left[1,2n\right]}$ de même longueur $h$. Nous allons, dans un premier temps, choisir l'espace 
$V_{h}=\left\{v\in \mathcal{C}^{0}(\overline{I}) : v_{|_{I_k}}\in \mathbb{P}_{2}(I_k)\ \forall k\in \left[1,2n\right]\right\}$,
$$
\mathbb{P}_{2}(I_k)=\left\{v_{|_{I_k}} \mid \exists (\alpha_{k}, \beta_{k}, \gamma_{k}) \in \mathbb{C}^{3}, \forall x \in I_{k}, v(x)=\alpha_{k} + \beta_{k}x + \gamma_{k}x^2\right\}
$$

$V_{h}$ se résume donc aux fonctions $\mathcal{C}^{0}(\overline{I})$ quadratiques par morceaux.
Définissons maintenant les fonctions de forme
$$
\begin{array}{l}
    \widehat{\varphi}_{1}(x)=2x^2-3x+1 \\
    \widehat{\varphi}_{2}(x)=4x(1-x) \\
    \widehat{\varphi}_{3}(x)=x(2x-1)
\end{array}
$$
Alors en notant $\widehat{x}_{1}=0$, $\widehat{x}_{2}=\frac{1}{2}$, $\widehat{x}_{3}=1$, quelles que soient les valeurs que l'on veut
se fixer à ces points, nous sommes capables de trouver une unique combinaison linéaire des fonctions de forme qui coïncide avec ces valeurs en ces points.
$$\forall (c_1,c_2,c_3)\in\mathbb{C}^{3}, \exists! (\alpha_1, \alpha_2, \alpha_3)\in\mathbb{C}^{3} \mid \sum_{j=1}^{3}\alpha_{j}\widehat{\varphi}_{j}(\widehat{x}_{k})=c_{k}, k=1,2,3$$
Puisque nous sommes en dimension finie, il suffit de montrer que l'application linéaire est injective et on vérifie que
$$c_{k}=0, k=1,2,3 \Longrightarrow \alpha_{k}=0, k=1,2,3$$
Il s'ensuit que les fonctions $(\varphi_{k})_{k\in\left[0,2n\right]}$ définies à l'aide des transformations affines $\tau_{k}(x)=\frac{x-x_{k-1}}{x_{k+1}-x_{k-1}}$ par
$$
\begin{array}{ll}
    \varphi_{2k+1}(x)&=\widehat{\varphi}_{2}\circ\tau_{2k+1}(x).\mathbbm{1}_{\left[0,1\right]}\circ\tau_{2k+1}(x)\\
    \varphi_{2k}(x)&=\widehat{\varphi}_{1}\circ\tau_{2k+1}(x).\mathbbm{1}_{\left[0,1\right]}\circ\tau_{2k+1}(x)+\widehat{\varphi}_{3}\circ\tau_{2k-1}(x).\mathbbm{1}_{\left[0,1\right]}\circ\tau_{2k-1}(x)\\
\end{array}
$$
forment une base de $V_{h}$. Il faut toutefois convenir que $x_{-2}=x_{0}-1$ et $x_{2n+2}=x_{2n}+1$ pour qu'il n'y ait pas de problème au bord.
\paragraph{}
Nous disposons donc à présent d'une base dans laquelle nous pouvons chercher à calculer une solution approchée à notre problème.
Nous pouvons noter que
$\text{supp}(\varphi_k)\cap\text{supp}(\varphi_j)=\emptyset$ pour $|j-k|\geq 2+\overline{r}_{k}$ (où $\overline{r}_{k}$ est le reste de la division euclidienne et $k+1$ par $2$).
Si l'on regarde de plus près la construction de la matrice des éléments finis dans (\ref{eq4}), on s'aperçoit que la plupart des éléments de la matrice valent $0$.
De plus, puisque la subdivision est équitablement répartie, les calculs d'aires sont identiques sur chaque sous-intervalle. Nous pouvons donc faire tous ces calculs à la main.
\subsubsection{Assemblage de la matrice des éléments finis}
Remarquons d'abord que sur $I_{2k}$ seules les fonctions $\varphi_{2k-2}, \varphi_{2k-1}, \varphi_{2k}$ vivent. De même sur 
$I_{2k+1}$ ce sont $\varphi_{2k}, \varphi_{2k+1}, \varphi_{2k+2}$.
En utilisant les transformations affines, il nous suffit donc de calculer
$$
K_0=\begin{pmatrix}
    ||\dx \widehat{\varphi}_{1}||_\Ldzu^2&(\dx \widehat{\varphi}_{2}, \dx \widehat{\varphi}_1)_\Ldzu&(\dx \widehat{\varphi}_{3}, \dx \widehat{\varphi}_1)_\Ldzu \\
    (\dx \widehat{\varphi}_{1}, \dx \widehat{\varphi}_2)_\Ldzu&||\dx \widehat{\varphi}_{2}||_\Ldzu^2&(\dx \widehat{\varphi}_{3}, \dx \widehat{\varphi}_2)_\Ldzu\\
    (\dx \widehat{\varphi}_{1}, \dx \widehat{\varphi}_3)_\Ldzu&(\dx \widehat{\varphi}_{2}, \dx \widehat{\varphi}_3)_\Ldzu&||\dx \widehat{\varphi}_{3}||_\Ldzu^2
\end{pmatrix}
$$
et
$$
M_0=\begin{pmatrix}
    ||\widehat{\varphi}_{1}||_\Ldzu^2&(\dx \widehat{\varphi}_{2}, \widehat{\varphi}_1)_\Ldzu&(\widehat{\varphi}_{3}, \widehat{\varphi}_1)_\Ldzu \\
    (\widehat{\varphi}_{1}, \widehat{\varphi}_2)_\Ldzu&||\widehat{\varphi}_{2}||_\Ldzu^2&(\widehat{\varphi}_{3}, \widehat{\varphi}_2)_\Ldzu\\
    (\widehat{\varphi}_{1}, \widehat{\varphi}_3)_\Ldzu&(\widehat{\varphi}_{2}, \widehat{\varphi}_3)_\Ldzu&||\widehat{\varphi}_{3}||_\Ldzu^2
\end{pmatrix}
$$
Finalement, nous avons
$$
K_0=\frac{1}{3}\begin{pmatrix}
    7&-8&1\\
    -8&16&-8\\
    1&-8&7
\end{pmatrix}
$$
$$
M_0=\frac{1}{30}\begin{pmatrix}
    4&2&-1\\
    2&16&2\\
    -1&2&4
\end{pmatrix}
$$
D'où l'on tire
$$
K=\frac{1}{6h}\begin{pmatrix}
    7&-8&1&0&\dots&\dots&\dots&0\\
    -8&16&-8&0&\ddots&&&\vdots\\
    1&-8&14&\ddots&1&\ddots&&\vdots\\
    0&0&\ddots&16&\ddots&\ddots&\ddots&\vdots\\
    \vdots&\ddots&1&\ddots&\ddots&\ddots&0&0\\
    \vdots&&\ddots&\ddots&\ddots&14&-8&1\\
    \vdots&&&\ddots&0&-8&16&-8\\
    0&\dots&\dots&\dots&0&1&-8&7
\end{pmatrix}
$$
$$
M=\frac{h}{15}\begin{pmatrix}
    4&2&-1&0&\dots&\dots&\dots&0\\
    2&16&2&0&\ddots&&&\vdots\\
    -1&2&8&\ddots&-1&\ddots&&\vdots\\
    0&0&\ddots&16&\ddots&\ddots&\ddots&\vdots\\
    \vdots&\ddots&-1&\ddots&\ddots&\ddots&0&0\\
    \vdots&&\ddots&\ddots&\ddots&8&2&-1\\
    \vdots&&&\ddots&0&2&16&2\\
    0&\dots&\dots&\dots&0&-1&2&4
\end{pmatrix}
$$

Nous avons donc $A=K-\omega^{2}M+i\omega B$ avec $B$ nulle partout sauf aux extrémités de la diagonale principale où elle prend la valeur $1$.
Il ne faut pas l'oublier, elle provient de l'intégrale sur le bord ! 
\begin{thebibliography}{1}
    \bibitem{DDHW}
    Francis Collino, Souad Ghanemi, Patrick Joly. \textit{Domain decomposition method for harmonic wave
    propagation: a general presentation}. INRIA, France. CERFACS, France.
\end{thebibliography}
\end{document}